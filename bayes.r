
ub.mix = function(x,start=c(.6,1,4)){
  #
  #The method of Allison et al. (2002) CSDA is used to model the
  #p-value distribution as a mixture of a uniform and another beta
  #distribution.  The mixing proportion on the uniform (pi0) is an
  #estimate of the proportion of true null hypotheses.
  #
  #This code is adapted from code provided by Gary Gadbury.
  #
  #The starting values were provided by Gary's group, but they
  #may need to be changed dependending on the problem.
  #Always check the fit of the estimated distribution using
  #the function plot.ub.mix.
  #
  #The function returns a vector that contains estimates of
  #pi0, alpha, and beta in that order.
  #
  mix.obj1<-function(p,x){
    # This object is the objective function for a mixture of a uniform
    # and one beta distribution
    #
    e=p[1] + (1-p[1])*(gamma(p[2]+p[3])/(gamma(p[2])*gamma(p[3])))*x^(p[2]-1)*(1-x)^(p[3]-1)
    sle=-sum(log(e))
    return(sle)
  }
  return(optim(start,mix.obj1,lower=c(.001,.01,.01),upper=c(1,170,170),method="L-BFGS-B",x = x)$par)
}

plot.ub.mix = function (p,parms=NULL,nclass=20){
  #
  #This function plots the estimated uniform-beta mixture density
  #over a histogram of p-values.
  #
  #The first argument p is a required vector of p-values.
  #The second argument is a vector which contains estimates
  #of pi0, alpha, and beta.  If no vector is supplied, one
  #will be estimated using the function ub.mix.  The third
  #argument specifies the number of bins for the histogram.
  #
  if(is.null(parms)){
    parms=ub.mix(p)
  }
  x=0:1000/1000
  hist(p,probability=T,col=4,nclass=nclass)
  box()
  lines(x,parms[1]+(1-parms[1])*dbeta(x,parms[2],parms[3]),col=2,lwd=2)
  lines(c(0,1),rep(parms[1],2),lty=2,col=2,lwd=2)
}

ppde=function (p,parms=NULL){
  #posterior probability of differentially expressed.
  #
  #p is a vector of p-values
  #parms is a vector containing estimates of pi0, alpha, and beta
  #from a uniform-beta mixture model fit the the p-values.
  #If parms is not specified, it will be estimated using ub.mix.
  #
  if(is.null(parms)){
    parms=ub.mix(p)
  }
  pi0=parms[1]
  pi1=1-pi0
  a=parms[2]
  b=parms[3]
  return(1/(pi0/(pi1*dbeta(p,a,b))+1))  
}
