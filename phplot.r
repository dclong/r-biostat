
#' histograms of pvalues for a given model
phplot = function(design.matrix,folder){
  dir.create(folder)
  #fit model to block 1
  lmFit(data.b1,design.matrix)->fit.b1
  #fit model to block 2
  lmFit(data.b2,design.matrix)->fit.b2
  #fit model to average of the two blocks
  lmFit(data.ave,design.matrix)->fit.ave
  #make a contrast
  makeContrasts(typewild,levels=design.matrix)->contr.mat
  #fit the contrast
  contrasts.fit(fit.b1,contr.mat)->fit.b1.wild
  contrasts.fit(fit.b2,contr.mat)->fit.b2.wild
  contrasts.fit(fit.ave,contr.mat)->fit.ave.wild
  #using bayes method
  eBayes(fit.b1.wild)->fit.b1.wild.ebayes
  eBayes(fit.b2.wild)->fit.b2.wild.ebayes
  eBayes(fit.ave.wild)->fit.ave.wild.ebayes
  #make plots
  hist(fit.b1.wild.ebayes$p.value,col="blue")
  box()
  dev.copy2pdf(file=combinePath(folder,"pvalue-b1.pdf"))
  hist(fit.b2.wild.ebayes$p.value,col="blue")
  box()
  dev.copy2pdf(file=combinePath(folder,"pvalue-b2.pdf"))
  hist(fit.ave.wild.ebayes$p.value,col="blue")
  box()
  dev.copy2pdf(file=combinePath(folder,"pvalue-ave.pdf"))
}