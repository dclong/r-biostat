#
#
#  R code for Bing's data
#
#
#  Tieming, 1/24/11
#
#

#setwd("/Users/Tieming/consulting/NSF-Bing-rice")
#read in data
d <- read.table("rma.summary.txt", header=TRUE, skip=98)
dim(d) # 57381 62    BY3 should bd dropped, use BY3redo instead
head(d)
boxplot(d[,-1])

#rename column names
h <- colnames(d)
hd <- unlist(strsplit(h[-1], split=c("_")))[seq(1,2*(length(h)-1),2)]
hd
hd <- c("probeid",hd)
colnames(d) <- hd
head(d)

#get rid of the bad chip
badchip <- (1:dim(d)[2])[hd=="BY3"]
d <- d[, -badchip]
colnames(d)[(1:dim(d)[2])[colnames(d)=="BY3redo"]]="BY3"
head(d)

chip.num <- as.numeric(unlist(strsplit(colnames(d)[-1], split="BY"))[seq(2,2*(dim(d)[2]-1),2)])
order.chip <- order(chip.num)

data <- data.frame(probeid=d[,1],d[,-1][,order.chip])
head(data)



#
# Data inspection.
#
# (1) Check Gene Osbzip, gene position "Os.49201.1.S1_at".
##################################################
#> data[11056,]
#               probeid  BY1  BY2  BY3  BY4  BY5 BY6  BY7  BY8 BY9 BY10 BY11
#11056 Os.49201.1.S1_at 3.33 3.46 3.45 3.58 3.77 3.9 3.77 3.81 3.9 3.58 3.77
#      BY12 BY13 BY14 BY15 BY16 BY17 BY18 BY19 BY20 BY21 BY22 BY23 BY24 BY25
#11056 4.19 4.13 2.97 3.44 3.24 3.57 3.83 5.09 3.46  3.4 4.66 3.22 3.34 3.46
#      BY26 BY27 BY28 BY29 BY30 BY31 BY32 BY33 BY34 BY35 BY36 BY37 BY38  BY39
#11056 4.33 3.85  3.3 3.66  3.5 3.31 3.95 3.52 3.86 3.78 3.62 9.53 7.02 10.28
#       BY40 BY41 BY42 BY43 BY44 BY45 BY46 BY47 BY48  BY49  BY50 BY51  BY52
#11056 10.09 6.38 5.82 7.31 9.15 5.73 9.73  8.7 7.76 10.98 10.49 9.57 10.48
#       BY53  BY54  BY55  BY56 BY57 BY58 BY59 BY60
#11056 10.77 10.09 10.79 11.33  8.6   10 9.12    7
###################################################
plot(rep(c(0,6,25,24,48),each=3), 
data[11056,c(2,3,4,14,15,16,26,27,28,38,39,40,50,51,52)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IR24")

plot(rep(c(0,6,25,24,48),each=3), 
data[11056,c(5,6,7,17,18,19,29,30,31,41,42,43,53,54,55)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB13")

plot(rep(c(0,6,25,24,48),each=3), 
data[11056,c(8,9,10,20,21,22,32,33,34,44,45,46,56,57,58)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB21")

plot(rep(c(0,6,25,24,48),each=3), 
data[11056,c(11,12,13,23,24,25,35,36,37,47,48,49,59,60,61)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB27")


# (2) Check Gene Os8N3, gene position "Os.10401.1.S1_s_at"
######################################################
#> data[2656,]
#                probeid  BY1 BY2  BY3  BY4 BY5  BY6  BY7  BY8  BY9 BY10 BY11
#2656 Os.10401.1.S1_s_at 3.15   4 3.85 4.09 3.4 3.76 3.51 3.86 3.97 3.66 3.56
#     BY12 BY13 BY14 BY15 BY16 BY17 BY18 BY19 BY20 BY21 BY22 BY23 BY24 BY25
#2656 3.28 5.96 5.87 5.45 5.74 5.62  6.5 6.96 4.62 7.63  7.5 7.68 7.46 4.48
#     BY26 BY27 BY28 BY29 BY30 BY31 BY32 BY33 BY34 BY35 BY36  BY37 BY38  BY39
#2656 5.72 5.59 4.12 4.24 3.96 4.22 5.52 4.99 4.46 5.17 4.19 11.68 9.03 12.37
#     BY40 BY41 BY42 BY43 BY44 BY45  BY46  BY47  BY48  BY49  BY50  BY51 BY52
#2656 3.72 4.36 4.13 9.16 10.9 7.46 11.66 10.88 10.14 12.84 12.68 12.17 4.03
#     BY53 BY54  BY55 BY56  BY57  BY58  BY59  BY60
#2656 4.54 3.81 12.37 12.8 10.51 12.42 12.16 10.15
#######################################################
plot(rep(c(0,6,25,24,48),each=3), 
data[2656,c(2,3,4,14,15,16,26,27,28,38,39,40,50,51,52)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IR24", ylim=range(data[2656,-1]))

plot(rep(c(0,6,25,24,48),each=3), 
data[2656,c(5,6,7,17,18,19,29,30,31,41,42,43,53,54,55)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB13", ylim=range(data[2656,-1]))

plot(rep(c(0,6,25,24,48),each=3), 
data[2656,c(8,9,10,20,21,22,32,33,34,44,45,46,56,57,58)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB21", ylim=range(data[2656,-1]))

plot(rep(c(0,6,25,24,48),each=3), 
data[2656,c(11,12,13,23,24,25,35,36,37,47,48,49,59,60,61)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB27", ylim=range(data[2656,-1]))


# (3) Check Gene OsTFIIAr1, gene position "OsAffx.24035.1.S1_s_at".
#############################################
#> data[42029,]
#                     probeid  BY1 BY2  BY3  BY4  BY5  BY6  BY7  BY8  BY9 BY10
#42029 OsAffx.24035.1.S1_s_at 7.86 7.8 8.12 7.84 7.97 8.29 7.66 8.14 8.37 7.68
#      BY11 BY12 BY13 BY14 BY15 BY16 BY17 BY18 BY19 BY20 BY21 BY22 BY23 BY24
#42029 8.31 7.86 8.53 8.23 8.36 8.38  8.3 8.37 9.12  7.9 8.39 8.93 8.65 8.33
#      BY25 BY26 BY27 BY28 BY29 BY30 BY31 BY32 BY33 BY34 BY35 BY36  BY37  BY38
#42029 7.81 7.98 7.98 7.84 7.99 7.99 7.85    8 8.09 7.66 7.96 8.05 11.66 10.01
#       BY39  BY40 BY41 BY42 BY43  BY44 BY45  BY46 BY47  BY48  BY49  BY50  BY51
#42029 11.81 11.84 9.49 9.07 10.2 11.23 8.78 11.93 11.3 10.44 12.44 12.08 11.31
#       BY52  BY53  BY54  BY55  BY56  BY57  BY58 BY59 BY60
#42029 11.99 12.17 11.46 12.21 12.62 10.52 11.96 11.5 9.87
##############################################
plot(rep(c(0,6,25,24,48),each=3), 
data[42029,c(2,3,4,14,15,16,26,27,28,38,39,40,50,51,52)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IR24", ylim=range(data[42029,-1]))

plot(rep(c(0,6,25,24,48),each=3), 
data[42029,c(5,6,7,17,18,19,29,30,31,41,42,43,53,54,55)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB13", ylim=range(data[42029,-1]))

plot(rep(c(0,6,25,24,48),each=3), 
data[42029,c(8,9,10,20,21,22,32,33,34,44,45,46,56,57,58)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB21", ylim=range(data[42029,-1]))

plot(rep(c(0,6,25,24,48),each=3), 
data[42029,c(11,12,13,23,24,25,35,36,37,47,48,49,59,60,61)], 
col=c(rep("blue", 6), rep("orange", 3), rep("blue", 6)),
xlab="Hour", ylab="Log of Normalized Microarray Intensity",
main="IRBB27", ylim=range(data[42029,-1]))





#
# design matrix.
#

genotype <- as.factor(
  rep(rep(c("IR24","IRBB13","IRBB21","IRBB27"), each=3), times=5))

time     <- as.factor(
  rep(c("0hr","6hr","24hrmock","24hr","48hr"), each=12))
  
trayrep.design <- read.table("tray-rep.txt",header=TRUE)
temp.order <- as.numeric(unlist(strsplit(as.character(trayrep.design$Chip),"BY"))[seq(2,2*dim(trayrep.design)[1],2)])
tray <- trayrep.design$Tray[order(temp.order)]
tray <- as.factor(tray)
rep  <- trayrep.design$Rep[order(temp.order)]
rep  <- as.factor(rep)




design <- model.matrix(~ genotype + time + genotype*time + rep)
colnames(design) <- c("mu","g13","g21","g27",
                      "t24","t24m","t48","t6",
                      "r2","r3",
                      #"tray2","tray3","tray4",
                      "g13t24","g21t24","g27t24",
                      "g13t24m","g21t24m","g27t24m",
                      "g13t48","g21t48","g27t48",
                      "g13t6","g21t6","g27t6")
design


#########################################################################################
#        0hr    6hr             24hrmock            24hr              48hr              #
# IR24   mu     mu+t6           mu+t24m             mu+t24            mu+t48            #
# IRBB13 mu+g13 mu+g13+t6+g13t6 mu+g13+t24m+g13t24m mu+g13+t24+g13t24 mu+g13+t48+g13t48 #
# IRBB27 mu+g27 mu+g27+t6+g27t6 mu+g27+t24m+g27t24m mu+g27+t24+g27t24 mu+g27+t48+g27t48 #
# IRBB21 mu+g21 mu+g21+t6+g21t6 mu+g21+t24m+g21t24m mu+g21+t24+g21t24 mu+g21+t48+g21t48 #
#########################################################################################

# Model selection.

design1 <- model.matrix(~ genotype + time + genotype*time)
design2 <- model.matrix(~ genotype + time + genotype*time + tray)
design3 <- model.matrix(~ genotype + time + genotype*time + rep)
design4 <- model.matrix(~ genotype + time + genotype*time + tray + rep)
temp <- selectModel(data[,-1], list(design2,design3,design4), criterion="aic")
table(temp$pref)
####################
#    1     2     3 
#21294 24198 11889 
####################
hist(as.numeric(temp$pref))
temp2 <- selectModel(data[,-1], list(design2,design3,design4), criterion="bic")
table(temp2$pref)
####################
#    1     2     3 
#15505 39657  2219 
####################
par(fig=c(0,1,0,1), cex.axis=1, mgp=c(3,1,0), mar=c(5,4,4,2))
design1.l <- lowess(1:dim(temp2$IC)[1], temp2$IC[,1], f=0.1)
design2.l <- lowess(1:dim(temp2$IC)[1], temp2$IC[,2], f=0.1)
design3.l <- lowess(1:dim(temp2$IC)[1], temp2$IC[,3], f=0.1)
plot(1:dim(temp2$IC)[1], temp2$IC[,1], ylim=range(c(design1.l$y, design2.l$y, design3.l$y)), xlab="Probes", ylab="BIC", type="n")
lines(design1.l$x, design1.l$y, col="black", lwd=2)
lines(design2.l$x, design2.l$y, col="red", lwd=2)
lines(design3.l$x, design3.l$y, col="green", lwd=2)
legend("topright",c("tray[15505]","rep[39657]","tray+rep[2219]"), col=c("black","red","green"), lwd=2, bty="n")
par(fig=c(2/15,7/15,2/15,7/15),new=T,cex.axis=0.6,mgp=c(5,0,0), mar=c(2,2,2,1), bty="o")
hist(as.numeric(temp2$pref), breaks=3, xlim=c(1,3),xlab="",ylab="", main="")

#
# LIMMA method to find differentially expressed genes.
#

library(limma)
fit <- lmFit(data[,-1], design)


#
# Test if we should put interaction term in the model.
# (We need the interaction term.)
#

contr.int = makeContrasts(tray2, tray3, tray4, levels=design)
fit.int = contrasts.fit(fit, contr.int)
res.int = eBayes(fit.int)
p.int = res.int$p.value
par(mfrow=c(2,2))
hist(p.int[,1],main="p values for Rep 2")
hist(p.int[,2],main="p values for Rep 3")
hist(p.int[,1],main="p values for Tray 2")
hist(p.int[,2],main="p values for Tray 3")
hist(p.int[,3],main="p values for Tray 4")
for (temp in 1:11)
{hist(p.int[,temp])}



#
# Comparison between two genotypes (one is IR24) at different time points.
#

q.value=
function(p, lambda=seq(0,0.95,0.05)) {
#
#This is Storey and Tibshirani's (PNAS, 2003) default method
#for determining the q-values.
#
#Code was originally obtained from John Storey's Web site.
#It has been edited slightly.
#
  m<-length(p) 
  pi0 <- rep(0,length(lambda))
  for(i in 1:length(lambda)) {
    pi0[i] <- mean(p >= lambda[i])/(1-lambda[i])
  }
  spi0 <- smooth.spline(lambda,pi0,df=3)
  pi0 <- max(predict(spi0,x=1)$y,0)
  pi0 <- min(pi0,1)
  u <- order(p)
  v <- rank(p)
  qvalue <- pi0*m*p/v
  qvalue[u[m]] <- min(qvalue[u[m]],1)
  for(i in (m-1):1) {
    qvalue[u[i]] <- min(qvalue[u[i]],qvalue[u[i+1]],1)
  }
  return(qvalue)
}




contr.0.trt=makeContrasts(g13, g27, g21,levels=design)
fit.0.trt=contrasts.fit(fit,contr.0.trt)
res.0.trt=eBayes(fit.0.trt)
p.0.trt=res.0.trt$p.value
par(mfrow=c(2,2))
hist(p.0.trt[,1], main="IR24 vs. IRBB13 at 0hr", xlab="p value")
hist(p.0.trt[,2], main="IR24 vs. IRBB27 at 0hr", xlab="p value")
hist(p.0.trt[,3], main="IR24 vs. IRBB21 at 0hr", xlab="p value")
q.0.trt <- apply(p.0.trt, 2, q.value)
head(q.0.trt)
cont.0.trt <- array(0, dim=dim(q.0.trt))
cont.0.trt[q.0.trt<=0.05] <- 1
vennDiagram(cont.0.trt,names=c("IRBB13","IRBB27","IRBB21"))
apply(q.0.trt<=0.05, 2, sum)  #151 450 245




contr.6.trt=makeContrasts(g13+g13t6, g27+g27t6, g21+g21t6,levels=design)
fit.6.trt=contrasts.fit(fit,contr.6.trt)
res.6.trt=eBayes(fit.6.trt)
p.6.trt=res.6.trt$p.value
par(mfrow=c(2,2))
hist(p.6.trt[,1], main="IR24 vs. IRBB13 at 6hr", xlab="p value")
hist(p.6.trt[,2], main="IR24 vs. IRBB27 at 6hr", xlab="p value")
hist(p.6.trt[,3], main="IR24 vs. IRBB21 at 6hr", xlab="p value")
q.6.trt <- apply(p.6.trt, 2, q.value)
head(q.6.trt)
cont.6.trt <- array(0, dim=dim(q.6.trt))
cont.6.trt[q.6.trt<=0.05] <- 1
vennDiagram(cont.6.trt,names=c("IRBB13","IRBB27","IRBB21"))
apply(q.6.trt<=0.05, 2, sum)
#
# g13 + g13t6 g27 + g27t6 g21 + g21t6 
#         179        1760         259 
#
topTable(res.6.trt, coef=3, number=20, adjust.method="BH", sort.by="none")
topTable(res.6.trt, coef=2, number=20, adjust.method="BH", sort.by="none")
?topTable


contr.24m.trt=makeContrasts(g13+g13t24m, g27+g27t24m, g21+g21t24m,levels=design)
fit.24m.trt=contrasts.fit(fit,contr.24m.trt)
res.24m.trt=eBayes(fit.24m.trt)
p.24m.trt=res.24m.trt$p.value
par(mfrow=c(2,2))
hist(p.24m.trt[,1], main="IR24 vs. IRBB13 at 24hr Mock", xlab="p value")
hist(p.24m.trt[,2], main="IR24 vs. IRBB27 at 24hr Mock", xlab="p value")
hist(p.24m.trt[,3], main="IR24 vs. IRBB21 at 24hr Mock", xlab="p value")
q.24m.trt <- apply(p.24m.trt, 2, q.value)
head(q.24m.trt)
cont.24m.trt <- array(0, dim=dim(q.24m.trt))
cont.24m.trt[q.24m.trt<=0.05] <- 1
vennDiagram(cont.24m.trt,names=c("IRBB13","IRBB27","IRBB21"))
apply(q.24m.trt<=0.05, 2, sum)  
#
# g13 + g13t24m g27 + g27t24m g21 + g21t24m 
#           103          1980           339 
#


contr.24.trt=makeContrasts(g13+g13t24, g27+g27t24, g21+g21t24,levels=design)
fit.24.trt=contrasts.fit(fit,contr.24.trt)
res.24.trt=eBayes(fit.24.trt)
p.24.trt=res.24.trt$p.value
par(mfrow=c(2,2))
hist(p.24.trt[,1], main="IR24 vs. IRBB13 at 24hr", xlab="p value")
hist(p.24.trt[,2], main="IR24 vs. IRBB27 at 24hr", xlab="p value")
hist(p.24.trt[,3], main="IR24 vs. IRBB21 at 24hr", xlab="p value")
q.24.trt <- apply(p.24.trt, 2, q.value)
head(q.24.trt)
cont.24.trt <- array(0, dim=dim(q.24.trt))
cont.24.trt[q.24.trt<=0.05] <- 1
vennDiagram(cont.24.trt,names=c("IRBB13","IRBB27","IRBB21"))
apply(q.24.trt<=0.05, 2, sum) 
#
# g13 + g13t24 g27 + g27t24 g21 + g21t24 
#          107          164          169 
#



contr.48.trt=makeContrasts(g13+g13t48, g27+g27t48, g21+g21t48,levels=design)
fit.48.trt=contrasts.fit(fit,contr.48.trt)
res.48.trt=eBayes(fit.48.trt)
p.48.trt=res.48.trt$p.value
par(mfrow=c(2,2))
hist(p.48.trt[,1], main="IR24 vs. IRBB13 at 48hr", xlab="p value")
hist(p.48.trt[,2], main="IR24 vs. IRBB27 at 48hr", xlab="p value")
hist(p.48.trt[,3], main="IR24 vs. IRBB21 at 48hr", xlab="p value")
q.48.trt <- apply(p.48.trt, 2, q.value)
head(q.48.trt)
cont.48.trt <- array(0, dim=dim(q.48.trt))
cont.48.trt[q.48.trt<=0.05] <- 1
vennDiagram(cont.48.trt,names=c("IRBB13","IRBB27","IRBB21"))
apply(q.48.trt<=0.05, 2, sum) 
#
# g13 + g13t48 g27 + g27t48 g21 + g21t48 
#          171          407          159 
#

# collect data.
#
# (1) For each genotype (IRBB13, IRBB21, IRBB27) vs. IR24, 
#     gene list with all time point estimated gene expressions.
# IRBB13 - IR24:
IRBB13vsIR24 <- data.frame(ID=data[,1])
IRBB13vsIR24$time0   <- res.0.trt$coefficients[,1]
IRBB13vsIR24$time6   <- res.6.trt$coefficients[,1]
IRBB13vsIR24$time24m <- res.24m.trt$coefficients[,1]
IRBB13vsIR24$time24  <- res.24.trt$coefficients[,1]
IRBB13vsIR24$time48  <- res.48.trt$coefficients[,1]
head(IRBB13vsIR24)
write.table(IRBB13vsIR24, "IRBB13vsIR24.txt")

de.13.24 <- IRBB13vsIR24[q.0.trt[,1]<=0.05 | q.6.trt[,1]<=0.05 | q.24.trt[,1]<=0.05 | 
            q.48.trt[,1]<=0.05 | q.24m.trt[,1] <= 0.05,]
dim(de.13.24)  # 414 6


color <- c("blue","lightblue","pink","black","black","black","orange","black",
           "black","black","black","black","black","black","black","black",
           "black","black","black","black","black","black","black","black",
           "black","black","black")


qt <- cbind(q.24m.trt[,1]<=0.05,q.0.trt[,1]<=0.05,q.6.trt[,1]<=0.05,
            q.24.trt[,1]<=0.05,q.48.trt[,1]<=0.05)[
            q.0.trt[,1]<=0.05 | q.6.trt[,1]<=0.05 | q.24.trt[,1]<=0.05 | 
            q.48.trt[,1]<=0.05 | q.24m.trt[,1] <= 0.05,]           
head(qt)
qt[qt=="FALSE"] <- "E"
qt[qt=="TRUE"]  <- "D"           
tt <- apply(qt, 1, paste, collapse="")           
pattern <- unique(tt)
pattern.count <- rep(0, times=length(pattern))

range(de.13.24[,-1])
par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.13.24$time0, de.13.24$time6, ylim=range(de.13.24[,-1]), xlim=c(-1,10), type="n",
     ylab="Expression Difference (IRBB13 - IR24)",xlab="Time", axes=FALSE, main="IRBB13 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.13.24)[1])){
  c <- color[tt[i] == pattern]
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.13.24$time24m[i],de.13.24$time0[i], 
        de.13.24$time6[i],de.13.24$time24[i],de.13.24$time48[i]), col=c)
  lines(c(-1.3, -1), c(de.13.24$time24m[i], de.13.24$time24m[i]))
}

text <- paste(pattern, pattern.count)[order(pattern.count, decreasing=TRUE)]
legend(x=8, y=5, legend=text, bty="n", cex=0.6, 
       col=color[order(pattern.count, decreasing=TRUE)], lwd=2)


par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.13.24$time0, de.13.24$time6, ylim=range(de.13.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB13 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB13 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.13.24)[1])){
  if(tt[i]==pattern[1]){
  lines(c(-1,0,1,4,8),c(de.13.24$time24m[i],de.13.24$time0[i], 
        de.13.24$time6[i],de.13.24$time24[i],de.13.24$time48[i]), col="blue")
  lines(c(-1.3, -1), c(de.13.24$time24m[i], de.13.24$time24m[i]))
  }
}

legend(x=1, y=5, legend="Expression Pattern EEDEE (106 genes)")



par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.13.24$time0, de.13.24$time6, ylim=range(de.13.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB13 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB13 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.13.24)[1])){
  if(tt[i]==pattern[7]){
  lines(c(-1,0,1,4,8),c(de.13.24$time24m[i],de.13.24$time0[i], 
        de.13.24$time6[i],de.13.24$time24[i],de.13.24$time48[i]), col="orange")
  lines(c(-1.3, -1), c(de.13.24$time24m[i], de.13.24$time24m[i]))
  }
}

legend(x=1, y=5, legend="Expression Pattern EEEED (96 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.13.24$time0, de.13.24$time6, ylim=range(de.13.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB13 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB13 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.13.24)[1])){
  if(tt[i]==pattern[3]){
  lines(c(-1,0,1,4,8),c(de.13.24$time24m[i],de.13.24$time0[i], 
        de.13.24$time6[i],de.13.24$time24[i],de.13.24$time48[i]), col="pink")
  lines(c(-1.3, -1), c(de.13.24$time24m[i], de.13.24$time24m[i]))
  }
}

legend(x=1, y=5, legend="Expression Pattern EDEEE (54 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.13.24$time0, de.13.24$time6, ylim=range(de.13.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB13 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB13 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.13.24)[1])){
  if(tt[i]==pattern[2]){
  lines(c(-1,0,1,4,8),c(de.13.24$time24m[i],de.13.24$time0[i], 
        de.13.24$time6[i],de.13.24$time24[i],de.13.24$time48[i]), col="lightblue")
  lines(c(-1.3, -1), c(de.13.24$time24m[i], de.13.24$time24m[i]))
  }
}

legend(x=1, y=5, legend="Expression Pattern DDDDD (43 genes)")






# IRBB27 - IR24:
IRBB27vsIR24 <- data.frame(ID=data[,1])
IRBB27vsIR24$time0   <- res.0.trt$coefficients[,2]
IRBB27vsIR24$time6   <- res.6.trt$coefficients[,2]
IRBB27vsIR24$time24m <- res.24m.trt$coefficients[,2]
IRBB27vsIR24$time24  <- res.24.trt$coefficients[,2]
IRBB27vsIR24$time48  <- res.48.trt$coefficients[,2]
head(IRBB27vsIR24)
write.table(IRBB27vsIR24, "IRBB27vsIR24.txt")


de.27.24 <- IRBB27vsIR24[q.0.trt[,2]<=0.05 | q.6.trt[,2]<=0.05 | q.24.trt[,2]<=0.05 | 
            q.48.trt[,2]<=0.05 | q.24m.trt[,2] <= 0.05,]
dim(de.27.24)  # 4144   6


color <- c("orange","blue","lightblue","pink","black","black","black","black",
           "black","black","black","black","black","black","black","black",
           "black","black","black","black","black","black","black","black",
           "black","black","black","black","black")
color[color=="black"] <- "lightgray"

qt <- cbind(q.24m.trt[,2]<=0.05,q.0.trt[,2]<=0.05,q.6.trt[,2]<=0.05,
            q.24.trt[,2]<=0.05,q.48.trt[,2]<=0.05)[
            q.0.trt[,2]<=0.05 | q.6.trt[,2]<=0.05 | q.24.trt[,2]<=0.05 | 
            q.48.trt[,2]<=0.05 | q.24m.trt[,2] <= 0.05,]           
head(qt)
qt[qt=="FALSE"] <- "E"
qt[qt=="TRUE"]  <- "D"           
tt <- apply(qt, 1, paste, collapse="")           
pattern <- unique(tt)   # 29 pattern
pattern.count <- rep(0, times=length(pattern))

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.27.24$time0, de.27.24$time6, ylim=range(de.27.24[,-1]), xlim=c(-1,10), type="n",
     ylab="Expression Difference (IRBB27 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB27 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)

for(i in 1:(dim(de.27.24)[1])){
  c <- color[tt[i] == pattern]
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.27.24$time24m[i],de.27.24$time0[i], 
        de.27.24$time6[i],de.27.24$time24[i],de.27.24$time48[i]), col=c)
  lines(c(-1.3, -1), c(de.27.24$time24m[i], de.27.24$time24m[i]))
}

text <- paste(pattern, pattern.count)[order(pattern.count, decreasing=TRUE)]
legend(x=8, y=8, legend=text, bty="n", cex=0.6, 
       col=color[order(pattern.count, decreasing=TRUE)], lwd=2)


par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.27.24$time0, de.27.24$time6, ylim=range(de.27.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB27 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB27 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)

for(i in 1:(dim(de.27.24)[1])){
  if(tt[i]==pattern[2]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.27.24$time24m[i],de.27.24$time0[i], 
        de.27.24$time6[i],de.27.24$time24[i],de.27.24$time48[i]), col="blue")
  lines(c(-1.3, -1), c(de.27.24$time24m[i], de.27.24$time24m[i]))
  }
}
legend(x=1, y=8, legend="Expression Pattern DEEEE (1712 genes)")


par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.27.24$time0, de.27.24$time6, ylim=range(de.27.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB27 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB27 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)

for(i in 1:(dim(de.27.24)[1])){
  if(tt[i]==pattern[1]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.27.24$time24m[i],de.27.24$time0[i], 
        de.27.24$time6[i],de.27.24$time24[i],de.27.24$time48[i]), col="orange")
  lines(c(-1.3, -1), c(de.27.24$time24m[i], de.27.24$time24m[i]))
  }
}
legend(x=1, y=8, legend="Expression Pattern EEDEE (1548 genes)")



par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.27.24$time0, de.27.24$time6, ylim=range(de.27.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB27 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB27 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)

for(i in 1:(dim(de.27.24)[1])){
  if(tt[i]==pattern[4]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.27.24$time24m[i],de.27.24$time0[i], 
        de.27.24$time6[i],de.27.24$time24[i],de.27.24$time48[i]), col="pink")
  lines(c(-1.3, -1), c(de.27.24$time24m[i], de.27.24$time24m[i]))
  }
}
legend(x=1, y=8, legend="Expression Pattern EEEED (257 genes)")


par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.27.24$time0, de.27.24$time6, ylim=range(de.27.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB27 - IR24)",xlab="Time", axes=FALSE, 
     main="IRBB27 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)

for(i in 1:(dim(de.27.24)[1])){
  if(tt[i]==pattern[3]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.27.24$time24m[i],de.27.24$time0[i], 
        de.27.24$time6[i],de.27.24$time24[i],de.27.24$time48[i]), col="lightblue")
  lines(c(-1.3, -1), c(de.27.24$time24m[i], de.27.24$time24m[i]))
  }
}
legend(x=1, y=8, legend="Expression Pattern EDEEE (248 genes)")




# IRBB21 - IR24:
IRBB21vsIR24 <- data.frame(ID=data[,1])
IRBB21vsIR24$time0   <- res.0.trt$coefficients[,3]
IRBB21vsIR24$time6   <- res.6.trt$coefficients[,3]
IRBB21vsIR24$time24m <- res.24m.trt$coefficients[,3]
IRBB21vsIR24$time24  <- res.24.trt$coefficients[,3]
IRBB21vsIR24$time48  <- res.48.trt$coefficients[,3]
head(IRBB21vsIR24)
write.table(IRBB21vsIR24, "IRBB21vsIR24.txt")


de.21.24 <- IRBB21vsIR24[q.0.trt[,3]<=0.05 | q.6.trt[,3]<=0.05 | q.24.trt[,3]<=0.05 | 
            q.48.trt[,3]<=0.05 | q.24m.trt[,3] <= 0.05,]
dim(de.21.24)  # 680 6


color <- c("lightgreen","lightblue","black","orange","black","blue","black","black",
           "pink","black","black","black","black","black","black","black",
           "black","black","black","black","black","black","black","black",
           "black","black","black","black","black","black","black")


qt <- cbind(q.24m.trt[,3]<=0.05,q.0.trt[,3]<=0.05,q.6.trt[,3]<=0.05,
            q.24.trt[,3]<=0.05,q.48.trt[,3]<=0.05)[
            q.0.trt[,3]<=0.05 | q.6.trt[,3]<=0.05 | q.24.trt[,3]<=0.05 | 
            q.48.trt[,3]<=0.05 | q.24m.trt[,3] <= 0.05,]           
head(qt)
qt[qt=="FALSE"] <- "E"
qt[qt=="TRUE"]  <- "D"           
tt <- apply(qt, 1, paste, collapse="")           
pattern <- unique(tt)
length(pattern)   # 31
pattern.count <- rep(0, times=length(pattern))

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,10), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  c <- color[tt[i] == pattern]
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col=c)
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
}

text <- paste(pattern, pattern.count)[order(pattern.count, decreasing=TRUE)]
legend(x=8, y=7, legend=text, bty="n", cex=0.7, 
       col=color[order(pattern.count, decreasing=TRUE)], lwd=2)

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  if(tt[i]==pattern[6]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col="blue")
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
  }
}
legend(x=1,y=6, legend="Expression Pattern DEEEE (188 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  if(tt[i]==pattern[6]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col="blue")
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
  }
}
legend(x=1,y=6, legend="Expression Pattern DEEEE (188 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  if(tt[i]==pattern[4]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col="orange")
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
  }
}
legend(x=1,y=6, legend="Expression Pattern EEDEE (131 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  if(tt[i]==pattern[2]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col="lightblue")
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
  }
}
legend(x=1,y=6, legend="Expression Pattern EDEEE (88 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  if(tt[i]==pattern[9]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col="pink")
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
  }
}
legend(x=1,y=6, legend="Expression Pattern DDDDD (67 genes)")

par(mar=c(4,4,3,1), mgp=c(2,1,0))
plot(de.21.24$time0, de.21.24$time6, ylim=range(de.21.24[,-1]), xlim=c(-1,8), type="n",
     ylab="Expression Difference (IRBB21 - IR24)",xlab="Time", axes=FALSE, main="IRBB21 vs. IR24")
abline(v=-1, col="gray", lwd=2)
abline(v=0, col="gray", lwd=2)
abline(v=1, col="gray", lwd=2)
abline(v=4, col="gray", lwd=2)
abline(v=8, col="gray", lwd=2)
text(c(-1,0,1,4,8)+0.25, par("usr")[3]-0.5, adj=1, xpd=TRUE,
    labels=c("24hr mock","0hr","6hr","24hr","48hr"), cex=0.8)
axis(2, cex=1)
for(i in 1:(dim(de.21.24)[1])){
  if(tt[i]==pattern[1]){
  pattern.count <- pattern.count + (tt[i]==pattern)
  lines(c(-1,0,1,4,8),c(de.21.24$time24m[i],de.21.24$time0[i], 
        de.21.24$time6[i],de.21.24$time24[i],de.21.24$time48[i]), col="lightgreen")
  lines(c(-1.3,-1), c(de.21.24$time24m[i],de.21.24$time24m[i]))
  }
}
legend(x=1,y=6, legend="Expression Pattern EEEED (41 genes)")




# For each time point, list of genes differentially expressed 
# between IRBB13, IRBB27, IRBB21 vs. IR24.
#
# 0hr:
time0hr <- data.frame(ID=data[,1])
time0hr <- cbind(time0hr,t=res.0.trt$t, p=p.0.trt, q=q.0.trt)

tt <- 2^topTable(res.0.trt, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time0hr$fc.g13 <- tt
tt <- 2^topTable(res.0.trt, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time0hr$fc.g27 <- tt
tt <- 2^topTable(res.0.trt, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time0hr$fc.g21 <- tt
write.table(time0hr, "time0hr.txt")





# 6hr:
time6hr <- data.frame(ID=data[,1])
time6hr <- cbind(time6hr, t=res.6.trt$t, p=p.6.trt, q=q.6.trt)
tt <- 2^topTable(res.6.trt, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time6hr$fc.g13 <- tt
tt <- 2^topTable(res.6.trt, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time6hr$fc.g27 <- tt
tt <- 2^topTable(res.6.trt, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time6hr$fc.g21 <- tt
colnames(time6hr) <- c("ID","t.g13","t.g27","t.g21","p.g13","p.g27","p.g21",
                       "q.g13","q.g27","q.g21","fc.g13","fc.g27","fc.g21")
head(time6hr)
write.table(time6hr, "time6hr.txt")


# 24hr:
time24hr <- data.frame(ID=data[,1])
time24hr <- cbind(time24hr, t=res.24.trt$t, p=p.24.trt, q=q.24.trt)
tt <- 2^topTable(res.24.trt, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24hr$fc.g13 <- tt
tt <- 2^topTable(res.24.trt, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24hr$fc.g27 <- tt
tt <- 2^topTable(res.24.trt, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24hr$fc.g21 <- tt
colnames(time24hr) <- c("ID","t.g13","t.g27","t.g21","p.g13","p.g27","p.g21",
                       "q.g13","q.g27","q.g21","fc.g13","fc.g27","fc.g21")
head(time24hr)
write.table(time24hr, "time24hr.txt")

# 48hr:
time48hr <- data.frame(ID=data[,1])
time48hr <- cbind(time48hr, t=res.48.trt$t, p=p.48.trt, q=q.48.trt)
tt <- 2^topTable(res.48.trt, coef=1, number=dim(data)[1], adjust.method="BH", 
      sort.by="none")$logFC
time48hr$fc.g13 <- tt
tt <- 2^topTable(res.48.trt, coef=2, number=dim(data)[1], adjust.method="BH", 
      sort.by="none")$logFC
time48hr$fc.g27 <- tt
tt <- 2^topTable(res.48.trt, coef=3, number=dim(data)[1], adjust.method="BH", 
      sort.by="none")$logFC
time48hr$fc.g21 <- tt
colnames(time48hr) <- c("ID","t.g13","t.g27","t.g21","p.g13","p.g27","p.g21",
                       "q.g13","q.g27","q.g21","fc.g13","fc.g27","fc.g21")
head(time48hr)
write.table(time48hr, "time48hr.txt")

# 24hr mock:
time24hrmock <- data.frame(ID=data[,1])
time24hrmock <- cbind(time24hrmock, t=res.24m.trt$t, p=p.24m.trt, q=q.24m.trt)
tt <- 2^topTable(res.24m.trt, coef=1, number=dim(data)[1], adjust.method="BH", 
                 sort.by="none")$logFC
time24hrmock$fc.g13 <- tt
tt <- 2^topTable(res.24m.trt, coef=2, number=dim(data)[1], adjust.method="BH", 
                 sort.by="none")$logFC
time24hrmock$fc.g27 <- tt
tt <- 2^topTable(res.24m.trt, coef=3, number=dim(data)[1], adjust.method="BH", 
                 sort.by="none")$logFC
time24hrmock$fc.g21 <- tt
colnames(time24hrmock) <- c("ID","t.g13","t.g27","t.g21","p.g13","p.g27","p.g21",
                            "q.g13","q.g27","q.g21","fc.g13","fc.g27","fc.g21")
head(time24hrmock)
write.table(time24hrmock, "time24hrmock.txt")



#
# Plot.
#

num.diff <- t(array(c(123, 255, 255, 140, 421, 223, 97, 2197, 376, 108, 188, 175, 265, 452, 161),
                  dim=c(3,5)))
num.diff
x <- c(0,6,24,48)
par(mar=c(5,5,2,2))
plot(x, num.diff[c(1,2,4,5),1], ylim=range(num.diff), pch=19,
     xlab="Time (hr)", ylab="No. of DE genes compared with IR24", cex.lab=1.3, cex.axis=1.3)
lines(x, num.diff[c(1,2,4,5),1], lty="dashed", lwd=2)
points(x, num.diff[c(1,2,4,5),2], pch=19, col="blue")
lines(x, num.diff[c(1,2,4,5),2], lty="dashed", lwd=2, col="blue")
points(x, num.diff[c(1,2,4,5),3], pch=19, col="red")
lines(x, num.diff[c(1,2,4,5),3], lty="dashed", lwd=2, col="red")
points(c(24,24,24), num.diff[3,], col=c("black","blue","red"), pch=7)
legend("topleft",c("IRBB13 vs. IR24","IRBB27 vs. IR24","IRBB21 vs. IR24",
       "IRBB13mock vs. IR24mock","IRBB27mock vs. IR24mock","IRBB21mock vs. IR24mock"),
       col=c("black","blue","red","black","blue","red"), lwd=2, 
       lty=c("dashed","dashed","dashed",NA,NA,NA), bty="n",
       pch=c(19,19,19,7,7,7),cex=1.3)



#
# Comparison between two resistant lines (no IR24).
#

#########################################################################################
#        0hr    6hr             24hr              24hrmock            48hr              #
# IR24   mu     mu+t6           mu+t24            mu+t24m             mu+t48            #
# IRBB13 mu+g13 mu+g13+t6+g13t6 mu+g13+t24+g13t24 mu+g13+t24m+g13t24m mu+g13+t48+g13t48 #
# IRBB27 mu+g27 mu+g27+t6+g27t6 mu+g27+t24+g27t24 mu+g27+t24m+g27t24m mu+g27+t48+g27t48 #
# IRBB21 mu+g21 mu+g21+t6+g21t6 mu+g21+t24+g21t24 mu+g21+t24m+g21t24m mu+g21+t48+g21t48 #
#########################################################################################

# 0hr:
contr.0=makeContrasts(g27-g13, g21-g13, g21-g27,levels=design)
fit.0=contrasts.fit(fit,contr.0)
res.0=eBayes(fit.0)
p.0=res.0$p.value
par(mfrow=c(2,2))
hist(p.0[,1], main="IRBB27 vs. IRBB13 at 0hr", xlab="p value")
hist(p.0[,2], main="IRBB21 vs. IRBB13 at 0hr", xlab="p value")
hist(p.0[,3], main="IRBB21 vs. IRBB27 at 0hr", xlab="p value")
q.0 <- apply(p.0, 2, q.value)
head(q.0)
cont.0 <- array(0, dim=dim(q.0))
cont.0[q.0<=0.05] <- 1
vennDiagram(cont.0,names=c("27-13","21-13","21-27"))
apply(q.0<=0.05, 2, sum)  #551 237 324

time0hr_resis <- data.frame(ID=data[,1])
time0hr_resis <- cbind(time0hr_resis,t=res.0$t, p=p.0, q=q.0)

tt <- 2^topTable(res.0, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time0hr_resis$fc.27.13 <- tt
tt <- 2^topTable(res.0, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time0hr_resis$fc.21.13 <- tt
tt <- 2^topTable(res.0, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time0hr_resis$fc.21.27 <- tt
head(time0hr_resis)
colnames(time0hr_resis) <- c("ID","t.27.13","t.21.13","t.21.27",
                             "p.27.13","p.21.13","p.21.27",
                             "q.27.13","q.21.13","q.21.27",
                             "fc.27.13","fc.21.13","fc.21.27")
write.table(time0hr_resis, "time0hr-resis.txt")



# 6hr:

contr.6=makeContrasts(g27-g13+g27t6-g13t6, 
                      g21-g13+g21t6-g13t6, 
                      g21-g27+g21t6-g27t6,levels=design)
fit.6=contrasts.fit(fit,contr.6)
res.6=eBayes(fit.6)
p.6=res.6$p.value
par(mfrow=c(2,2))
hist(p.6[,1], main="IRBB27 vs. IRBB13 at 6hr", xlab="p value")
hist(p.6[,2], main="IRBB21 vs. IRBB13 at 6hr", xlab="p value")
hist(p.6[,3], main="IRBB21 vs. IRBB27 at 6hr", xlab="p value")
q.6 <- apply(p.6, 2, q.value)
head(q.6)
cont.6 <- array(0, dim=dim(q.6))
cont.6[q.6<=0.05] <- 1
vennDiagram(cont.6,names=c("27-13","21-13","21-27"))
apply(q.6<=0.05, 2, sum)  # 451 208 91


time6hr_resis <- data.frame(ID=data[,1])
time6hr_resis <- cbind(time6hr_resis, t=res.6$t, p=p.6, q=q.6)

tt <- 2^topTable(res.6, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time6hr_resis$fc.27.13 <- tt
tt <- 2^topTable(res.6, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time6hr_resis$fc.21.13 <- tt
tt <- 2^topTable(res.6, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time6hr_resis$fc.21.27 <- tt
head(time6hr_resis)
colnames(time6hr_resis) <- c("ID","t.27.13","t.21.13","t.21.27",
                             "p.27.13","p.21.13","p.21.27",
                             "q.27.13","q.21.13","q.21.27",
                             "fc.27.13","fc.21.13","fc.21.27")
write.table(time6hr_resis, "time6hr-resis.txt")


# 24hr:

contr.24=makeContrasts(g27-g13+g27t24-g13t24, 
                       g21-g13+g21t24-g13t24, 
                       g21-g27+g21t24-g27t24,levels=design)
fit.24=contrasts.fit(fit, contr.24)
res.24=eBayes(fit.24)
p.24=res.24$p.value
par(mfrow=c(2,2))
hist(p.24[,1], main="IRBB27 vs. IRBB13 at 24hr", xlab="p value")
hist(p.24[,2], main="IRBB21 vs. IRBB13 at 24hr", xlab="p value")
hist(p.24[,3], main="IRBB21 vs. IRBB27 at 24hr", xlab="p value")
q.24 <- apply(p.24, 2, q.value)
head(q.24)
cont.24 <- array(0, dim=dim(q.24))
cont.24[q.24<=0.05] <- 1
vennDiagram(cont.24, names=c("27-13","21-13","21-27"))
apply(q.24<=0.05, 2, sum)  # 229 166 178


time24hr_resis <- data.frame(ID=data[,1])
time24hr_resis <- cbind(time24hr_resis, t=res.24$t, p=p.24, q=q.24)

tt <- 2^topTable(res.24, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24hr_resis$fc.27.13 <- tt
tt <- 2^topTable(res.24, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24hr_resis$fc.21.13 <- tt
tt <- 2^topTable(res.24, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24hr_resis$fc.21.27 <- tt
head(time24hr_resis)
colnames(time24hr_resis) <- c("ID","t.27.13","t.21.13","t.21.27",
                              "p.27.13","p.21.13","p.21.27",
                              "q.27.13","q.21.13","q.21.27",
                              "fc.27.13","fc.21.13","fc.21.27")
write.table(time24hr_resis, "time24hr-resis.txt")


# 24hr mock:


contr.24m=makeContrasts(g27-g13+g27t24m-g13t24m, 
                        g21-g13+g21t24m-g13t24m, 
                        g21-g27+g21t24m-g27t24m,levels=design)
fit.24m=contrasts.fit(fit, contr.24m)
res.24m=eBayes(fit.24m)
p.24m=res.24m$p.value
par(mfrow=c(2,2))
hist(p.24m[,1], main="IRBB27 vs. IRBB13 at 24hr mock", xlab="p value")
hist(p.24m[,2], main="IRBB21 vs. IRBB13 at 24hr mock", xlab="p value")
hist(p.24m[,3], main="IRBB21 vs. IRBB27 at 24hr mock", xlab="p value")
q.24m <- apply(p.24m, 2, q.value)
head(q.24m)
cont.24m <- array(0, dim=dim(q.24m))
cont.24m[q.24m<=0.05] <- 1
vennDiagram(cont.24m, names=c("27-13","21-13","21-27"))
apply(q.24m<=0.05, 2, sum)  # 2611 283 108


time24mhr_resis <- data.frame(ID=data[,1])
time24mhr_resis <- cbind(time24mhr_resis, t=res.24m$t, p=p.24m, q=q.24m)

tt <- 2^topTable(res.24m, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24mhr_resis$fc.27.13 <- tt
tt <- 2^topTable(res.24m, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24mhr_resis$fc.21.13 <- tt
tt <- 2^topTable(res.24m, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time24mhr_resis$fc.21.27 <- tt
head(time24mhr_resis)
colnames(time24mhr_resis) <- c("ID","t.27.13","t.21.13","t.21.27",
                              "p.27.13","p.21.13","p.21.27",
                              "q.27.13","q.21.13","q.21.27",
                              "fc.27.13","fc.21.13","fc.21.27")
write.table(time24mhr_resis, "time24hrmock-resis.txt")



# 48hr:

contr.48=makeContrasts(g27-g13+g27t48-g13t48, 
                       g21-g13+g21t48-g13t48, 
                       g21-g27+g21t48-g27t48,levels=design)
fit.48=contrasts.fit(fit, contr.48)
res.48=eBayes(fit.48)
p.48=res.48$p.value
par(mfrow=c(2,2))
hist(p.48[,1], main="IRBB27 vs. IRBB13 at 48hr", xlab="p value")
hist(p.48[,2], main="IRBB21 vs. IRBB13 at 48hr", xlab="p value")
hist(p.48[,3], main="IRBB21 vs. IRBB27 at 48hr", xlab="p value")
q.48 <- apply(p.48, 2, q.value)
head(q.48)
cont.48 <- array(0, dim=dim(q.48))
cont.48[q.48<=0.05] <- 1
vennDiagram(cont.48, names=c("27-13","21-13","21-27"))
apply(q.48<=0.05, 2, sum) #  1158 346 557

time48hr_resis <- data.frame(ID=data[,1])
time48hr_resis <- cbind(time48hr_resis, t=res.48$t, p=p.48, q=q.48)

tt <- 2^topTable(res.48, coef=1, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time48hr_resis$fc.27.13 <- tt
tt <- 2^topTable(res.48, coef=2, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time48hr_resis$fc.21.13 <- tt
tt <- 2^topTable(res.48, coef=3, number=dim(data)[1], adjust.method="BH", sort.by="none")$logFC
time48hr_resis$fc.21.27 <- tt
head(time48hr_resis)
colnames(time48hr_resis) <- c("ID","t.27.13","t.21.13","t.21.27",
                              "p.27.13","p.21.13","p.21.27",
                              "q.27.13","q.21.13","q.21.27",
                              "fc.27.13","fc.21.13","fc.21.27")
write.table(time48hr_resis, "time48hr-resis.txt")











