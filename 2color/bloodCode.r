

library(limma)

bloodTargets=readTargets("Blood/bloodTargets.txt")
bloodTargets

bloodRG=read.maimages(bloodTargets, source="agilent", path = "Blood",
                 names=paste(rep(c("uc","cu"),each=4),rep(1:4,2),sep=""))

attributes(bloodRG)

head(bloodRG$R)

head(bloodRG$G)


######
#
#Examine boxplots of red and green backgrounds.
#
######

boxplot(data.frame(cbind(log2(bloodRG$Gb),log2(bloodRG$Rb))),
        main="Background",col=rep(c("green","red"),each=8))


boxplot(log2(data.frame(bloodRG$R,bloodRG$G))[,as.vector(rbind(1:8,9:16))],
         col=rep(c("red","green"),8))


######
#
#Perform background correction for all slides.
#
######

bloodRGbc=backgroundCorrect(bloodRG,method="normexp")

######
#
#Examine side-by-side boxplots of background corrected data.
#
######

boxplot(log2(data.frame(bloodRGbc$R,bloodRGbc$G))[,as.vector(rbind(1:8,9:16))],
         col=rep(c("red","green"),8))

######
#
#Examine background corrected signal densities for each channel.
#
######

plotDensities(bloodRGbc)

######
#
#Examine plots of the difference vs. the average
#log background corrected signal.
#
######

plotMA(bloodRGbc)
plotMA(bloodRGbc[,2])
plotMA(bloodRGbc[,3])
plotMA(bloodRGbc[,4])
plotMA(bloodRGbc[,5])
plotMA(bloodRGbc[,6])

######
#
#Loess normalize and median center the data.
#Examine the resulting Red-Green differences.
#
######

bloodMA=normalizeWithinArrays(bloodRGbc,method="loess")
bloodMA=normalizeWithinArrays(bloodMA,method="median")

attributes(bloodMA)
head(bloodMA$M)
head(bloodMA$A)

plotMA(bloodMA[,8])
plotDensities(bloodMA)
boxplot(data.frame(bloodMA$M))

######
#
#Scale normalize the differences.
#
######

bloodMA=normalizeBetweenArrays(bloodMA,method="scale")
boxplot(data.frame(bloodMA$M))

######
#
#Analyze data.
#
######

#Create a design matrix for a gene-specific vector of
#differences as the response.

bloodTargets

a=gl(2,4)
b=factor(rep(1:4,2))


bloodDesign=model.matrix(~a+b)
bloodDesign

colnames(bloodDesign)=c("delta","trt","B2","B3","B4")
bloodDesign

#Use the limma function lmFit to fit the model.

bloodFit=lmFit(bloodMA, bloodDesign)

#Set up contrasts and compute p-values using the limma approach.

#Tests are t-u, t-c, and u-c (last two confounded with dye difference).

bloodContrastMatrix=makeContrasts(delta,trt,levels=bloodDesign)

bloodFit2=contrasts.fit(bloodFit, bloodContrastMatrix)
bloodEBayesFit=eBayes(bloodFit2)
attributes(bloodEBayesFit)

#Examine estimates of the prior df and variance.

bloodEBayesFit$df.prior
bloodEBayesFit$s2.prior

#Compare variance estimates before and after shrinkage.

plot(log(bloodEBayesFit$sigma),log(sqrt(bloodEBayesFit$s2.post)),
     xlab="Log Sqrt Original Variance Estimate",
     ylab="Log Sqrt Empirical Bayes Variance Estimate")
lines(c(-99,99),c(-99,99),col=2)
lsrs20=log(sqrt(bloodEBayesFit$s2.prior))
abline(h=lsrs20,col=4)
abline(v=lsrs20,col=4)

dim(bloodEBayesFit$p.value)
bloodPvalues=bloodEBayesFit$p.value
colnames(bloodPvalues)=c("delta","trt")

hist(bloodPvalues[,1],col=4)
box()

hist(bloodPvalues[,2],col=4)
box()


#Get log base 2 fold change estimates

bloodLog2FC=bloodEBayesFit$coefficients

#Create a volcano plot.
plot(bloodLog2FC[,1],-log(bloodPvalues[,1],base=10),
     xlab=expression(paste(log[2]," fold change")),
     ylab=expression(paste(-log[10]," p-value")),
     cex.lab=1.7)

plot(bloodLog2FC[,2],-log(bloodPvalues[,2],base=10),
     xlab=expression(paste(log[2]," fold change")),
     ylab=expression(paste(-log[10]," p-value")),
     cex.lab=1.7)

#Load some functions for multiple testing.

source("http://www.public.iastate.edu/~dnett/microarray/multtest.txt")

#Estimate the number of EE (m0) and DE (m1) genes.

bloodM=nrow(bloodPvalues)
bloodM0=estimate.m0(bloodPvalues[,1])
bloodM1=bloodM-bloodM0
bloodM
bloodM0
bloodM1

bloodM=nrow(bloodPvalues)
bloodM0=estimate.m0(bloodPvalues[,2])
bloodM1=bloodM-bloodM0
bloodM
bloodM0
bloodM1

bloodM=nrow(bloodPvalues)
bloodM0=estimate.m0(neuronPvalues[,3])
bloodM1=bloodM-bloodM0
bloodM
bloodM0
bloodM1

#Convert p-values to q-values.

bloodQvalues=apply(bloodPvalues,2,jabes.q)

#Find number of genes declared to be DE
#for various FDR thresholds.

fdrCuts=c(0.001,0.01,0.05,.1,.15,.2,.25)
cbind(fdrCuts,NumberOfGenes=apply(outer(bloodQvalues[,1],fdrCuts,"<="),2,sum))
cbind(fdrCuts,NumberOfGenes=apply(outer(bloodQvalues[,2],fdrCuts,"<="),2,sum))
cbind(fdrCuts,NumberOfGenes=apply(outer(bloodQvalues[,3],fdrCuts,"<="),2,sum))

bloodPCs=prcomp(t(bloodMA$M),center=T,scale.=T,retx=T)
plot(bloodPCs$x[,1],bloodPCs$x[,2],pch=16,col=rep(1:2,each=4))

###########
###########
#Now do separate analyses for each comparison
###########
###########

####treated - untreated######

bloodDiff=bloodMA$M[,1:4]-bloodMA$M[,5:8]

bloodDesign2=matrix(rep(1,4),nrow=4)
bloodDesign2

colnames(bloodDesign2)=c("tu")
bloodDesign2

#Use the limma function lmFit to fit the model.

bloodFitDiff=lmFit(bloodDiff, bloodDesign2)

#Set up contrasts and compute p-values using the limma approach.

bloodContrastMatrix2=makeContrasts(tu,levels=bloodDesign2)

bloodFitDiff2=contrasts.fit(bloodFitDiff, bloodContrastMatrix2)
bloodEBayesFitDiff=eBayes(bloodFitDiff2)

bloodPtu=bloodEBayesFitDiff$p.value[,1]
bloodLM2FCtu=bloodEBayesFitDiff$coefficients[,1]


plot(bloodPvalues[,1],bloodPtu)
plot(bloodLM2FCtu,-log(bloodPtu,10))

bloodDiff[bloodLM2FCtu>3,]

####treated - carrier######

bloodDifftc=bloodMA$M[,1:4]

bloodDesign3=matrix(rep(1,4),nrow=4)
bloodDesign3

colnames(bloodDesign3)=c("tc")
bloodDesign3

#Use the limma function lmFit to fit the model.

bloodFitDifftc=lmFit(bloodDifftc, bloodDesign3)

#Set up contrasts and compute p-values using the limma approach.

bloodContrastMatrix=makeContrasts(tc,levels=bloodDesign3)

bloodFitDifftc2=contrasts.fit(bloodFitDifftc, bloodContrastMatrix)
bloodEBayesFitDifftc=eBayes(bloodFitDifftc2)

bloodPtc=bloodEBayesFitDifftc$p.value[,1]

plot(bloodPtc,bloodPvalues[,2])
l2fctc=bloodEBayesFitDifftc$coefficients[,1]
plot(l2fctc,-log(bloodPtc,10))

####untreated - carrier######

d=MA$M[,5:8]

design=matrix(rep(1,4),nrow=4)
design

colnames(design)=c("uc")
design

#Use the limma function lmFit to fit the model.

fitd=lmFit(d, design)

#Set up contrasts and compute p-values using the limma approach.

contrast.matrix=makeContrasts(uc,levels=design)

fitd2=contrasts.fit(fitd, contrast.matrix)
efitd=eBayes(fitd2)

puc=efitd$p.value[,1]
l2fcuc=efitd$coefficients[,1]

plot(-log(puc,10),-log(p[,3],10))

plot(l2fcuc,-log(puc,10))

#########################


qtu=jabes.q(ptu)
qtc=jabes.q(ptc)
quc=jabes.q(puc)

fdrcuts=seq(0.01,.15,by=0.01)
cbind(fdrcuts,NumberOfGenes=apply(outer(qtu,fdrcuts,"<="),2,sum))
cbind(fdrcuts,NumberOfGenes=apply(outer(qtc,fdrcuts,"<="),2,sum))
cbind(fdrcuts,NumberOfGenes=apply(outer(quc,fdrcuts,"<="),2,sum))



estimate.m0(puc)
estimate.m0(ptc)
estimate.m0(ptu)


ouc=ub.mix(puc,c(.5,1,10))
plot.ub.mix(puc,ouc)

otc=ub.mix(ptc,c(.5,1,10))
plot.ub.mix(ptc,otc)

otu=ub.mix(ptu,c(.7,2,5))
plot.ub.mix(ptu,otu)

ppdeuc=ppde(puc,ouc)
ppdetc=ppde(ptc,otc)
ppdetu=ppde(ptu,otu)
max(ppdetu)















#Examine position of genes declared to be DE at FDR=0.01
#in MA plot for slide 6.

plot(MA$A[,6],MA$M[,6],
     col=1+(qvals<=0.01),
     pch=3+14*(qvals<=0.01),cex=.6)

#Fit uniform-beta mixture model.

out=ub.mix(p)
out

plot.ub.mix(p,out)

#Change starting values and refit
#uniform-beta mixture model.

out=ub.mix(p,c(.5,1,10))
out

plot.ub.mix(p,out)

#Compute estimated ppde for each gene.

eppde=ppde(p,out)

plot(p,eppde,xlab="p-value",
             ylab="Estimated Posterior Probability of DE",
             cex.lab=1.5)

ppdecuts=seq(.6,.95,by=0.05)
cbind(ppdecuts,NumberOfGenes=apply(outer(eppde,ppdecuts,">="),2,sum))
