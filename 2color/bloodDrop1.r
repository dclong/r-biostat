

library(limma)

bloodTargets=readTargets("blood/bloodTargets.txt")
bloodTargets

bloodRG=read.maimages(bloodTargets, source="agilent", path = "blood",
                 names=paste(rep(c("uc","cu"),each=4),rep(1:4,2),sep=""))

attributes(bloodRG)

head(bloodRG$R)

head(bloodRG$G)

#data frame for background, logarithm
data.frame(cbind(log2(bloodRG$Rb),log2(bloodRG$Gb)))->bloodRG.bg.log2
#data frame for signals, logarithm
data.frame(cbind(log2(bloodRG$R),log2(bloodRG$G)))->bloodRG.signal.log2

#Examine boxplots of red and green backgrounds.
boxplot(bloodRG.bg.log2,main="Background",col=rep(c("red","green"),each=8))


boxplot(bloodRG.signal.log2[,as.vector(rbind(1:8,9:16))],col=rep(c("red","green"),8))

dropIndex = bloodRG.bg.log2>8 & (bloodRG.signal.log2<bloodRG.bg.log2)

#histogram showing which points (genes) are dropped

for(i in 1:16){
	temp = bloodRG.bg.log2[,i][dropIndex[,i]]
	points(x=rep(i,length(temp)),temp,col="red")
}
abline(h=8,col="blue")

#change the drop index to drop index of genes
geneDropBool = apply(dropIndex,1,any)

#make the histogram after drop some "bad" genes 
boxplot(bloodRG.bg.log2[!geneDropBool,],
main="Background after Bad Gene Dropped",col=rep(c("red","green"),each=8))

#mark these bad points as red
for(i in 1:16){
	temp = bloodRG.bg.log2[,i][geneDropIndex]
	points(x=rep(i,length(temp)),temp,col="red")	
}

#find the index of bad genes
geneDropIndex = which(geneDropBool)

#mark these bad genes in different colors
for(i in 1:length(geneDropIndex)){
	points(1:16,bloodRG.bg.log2[geneDropIndex[i],],col=i+1,pch=i+1)
}


#drop these record associated with the bad genes
bloodRG.keep = bloodRG[-geneDropIndex,]

######
#
#Perform background correction for all slides.
#
######

bloodRG.keep.bc=backgroundCorrect(bloodRG.keep,method="normexp")

######
#
#Examine side-by-side boxplots of background corrected data.
#
######

boxplot(log2(data.frame(bloodRG.keep.bc$R,bloodRG.keep.bc$G))[,as.vector(rbind(1:8,9:16))],
         col=rep(c("red","green"),8),main="Signal After Background Corrected for Good Genes")
#save the plot to pdf
dev.copy2pdf(file="signal.pdf")
dev.off()

######
#
#Examine background corrected signal densities for each channel.
#
######

plotDensities(bloodRG.keep.bc)
dev.copy2pdf(file="densityPlot.pdf")
dev.off()
#I'm here!
######
#
#Examine plots of the difference vs. the average
#log background corrected signal.
#
######

plotMA(bloodRG.keep.bc[,1],main="MA Plot for Slide 1\nafter Background Correcting")
dev.copy2pdf(file="maplot-s1.pdf")
plotMA(bloodRG.keep.bc[,2],main="MA Plot for Slide 2\nafter Background Correcting")
dev.copy2pdf(file="maplot-s2.pdf")
plotMA(bloodRG.keep.bc[,3],main="MA Plot for Slide 3\nafter Background Correcting")
dev.copy2pdf(file="maplot-s3.pdf")
plotMA(bloodRG.keep.bc[,4],main="MA Plot for Slide 4\nafter Background Correcting")
dev.copy2pdf(file="maplot-s4.pdf")
plotMA(bloodRG.keep.bc[,5],main="MA Plot for Slide 5\nafter Background Correcting")
dev.copy2pdf(file="maplot-s5.pdf")
plotMA(bloodRG.keep.bc[,6],main="MA Plot for Slide 6\nafter Background Correcting")
dev.copy2pdf(file="maplot-s6.pdf")

######
#
#Loess normalize and median center the data.
#Examine the resulting Red-Green differences.
#
######

bloodMA.keep=normalizeWithinArrays(bloodRG.keep.bc,method="loess")
bloodMA.keep=normalizeWithinArrays(bloodMA.keep,method="median")

attributes(bloodMA.keep)
head(bloodMA.keep$M)
head(bloodMA.keep$A)

plotMA(bloodMA.keep[,8],main="MA plot for Slide 8 after Normalizing")
dev.copy2pdf(file="maplot-norm-s8.pdf")
plotDensities(bloodMA.keep)
dev.copy2pdf(file="denPlot-norm.pdf")
boxplot(data.frame(bloodMA.keep$M),main="Average Signal after Normalizing")
dev.copy2pdf(file="boxPlot-ave-norm.pdf")
######
#
#Scale normalize the differences.
#
######

bloodMA.keep=normalizeBetweenArrays(bloodMA.keep,method="scale")
boxplot(data.frame(bloodMA.keep)$M))

######
#
#Analyze data.
#
######

#Create a design matrix for a gene-specific vector of
#differences as the response.

bloodTargets

bloodDataA = (bloodMA.keep$M[,1:4] + bloodMA.keep$M[,5:8])/2
bloodDataD = (bloodMA.keep$M[,1:4] - bloodMA.keep$M[,5:8])/2

bloodDesignA=matrix(1,nrow=4)
bloodDesignA
colnames(bloodDesignA) = "Cy5Cy3"
bloodDesignA

bloodDesignD = matrix(1,nrow=4)
bloodDesignD
colnames(bloodDesignD) = "cu"
bloodDesignD

#Use the limma function lmFit to fit the model.

bloodFitA=lmFit(bloodDataA, bloodDesignA)
bloodFitD=lmFit(bloodDataD, bloodDesignD)
#Set up contrasts and compute p-values using the limma approach.

#Tests are cy5cy3 and cu 

bloodContrastMatrix.A=makeContrasts(Cy5Cy3,levels=bloodDesignA)
bloodContrastMatrix.D=makeContrasts(cu,levels=bloodDesignD)

bloodFitA2=contrasts.fit(bloodFitA, bloodContrastMatrix.A)
bloodFitD2=contrasts.fit(bloodFitD, bloodContrastMatrix.D)

bloodEBayesFitA=eBayes(bloodFitA2)
bloodEBayesFitD=eBayes(bloodFitD2)

attributes(bloodEBayesFitA)
attributes(bloodEBayesFitD)
#Examine estimates of the prior df and variance.

bloodEBayesFitA$df.prior
bloodEBayesFitA$s2.prior

bloodEBayesFitD$df.prior
bloodEBayesFitD$s2.prior
#Compare variance estimates before and after shrinkage.

plot(log(bloodEBayesFitA$sigma),log(sqrt(bloodEBayesFitA$s2.post)),
     xlab="Log Sqrt Original Variance Estimate",
     ylab="Log Sqrt Empirical Bayes Variance Estimate",
     main="Dye Effect Model")
lines(c(-99,99),c(-99,99),col=2)
lsrs20=log(sqrt(bloodEBayesFitA$s2.prior))
abline(h=lsrs20,col=4)
abline(v=lsrs20,col=4)
dev.copy2pdf(file="variancePlotA.pdf")
dev.off()

plot(log(bloodEBayesFitD$sigma),log(sqrt(bloodEBayesFitD$s2.post)),
     xlab="Log Sqrt Original Variance Estimate",
     ylab="Log Sqrt Empirical Bayes Variance Estimate",
     main="CU Effect Model")
lines(c(-99,99),c(-99,99),col=2)
lsrs20=log(sqrt(bloodEBayesFitD$s2.prior))
abline(h=lsrs20,col=4)
abline(v=lsrs20,col=4)
dev.copy2pdf(file="variancePlotD.pdf")
dev.off()

dim(bloodEBayesFitA$p.value)
bloodPvalues.A=bloodEBayesFitA$p.value[,1]
dim(bloodEBayesFitD$p.value)
bloodPvalues.D=bloodEBayesFitD$p.value[,1]

hist(bloodPvalues.A,col=4)
box()
dev.copy2pdf(file="pHistA.pdf")
dev.off()

hist(bloodPvalues.D,col=4)
box()
dev.copy2pdf(file="pHistB.pdf")
dev.off()
#Get log base 2 fold change estimates

bloodLog2FC.A=bloodEBayesFitA$coefficients
bloodLog2FC.D=bloodEBayesFitD$coefficients
#Create a volcano plot.
plot(bloodLog2FC.A,-log(bloodPvalues.A,base=10),
     xlab=expression(paste(log[2]," fold change")),
     ylab=expression(paste(-log[10]," p-value")),
     cex.lab=1.7)
dev.copy2pdf(file="vPlotA.pdf")

plot(bloodLog2FC.D,-log(bloodPvalues.D,base=10),
     xlab=expression(paste(log[2]," fold change")),
     ylab=expression(paste(-log[10]," p-value")),
     cex.lab=1.7)
dev.copy2pdf(file="vPlotD.pdf")
#Load some functions for multiple testing.

source("http://www.public.iastate.edu/~dnett/microarray/multtest.txt")

#Estimate the number of EE (m0) and DE (m1) genes.

blood.m=length(bloodPvalues.A)
blood.m0.A=estimate.m0(bloodPvalues.A)
blood.m1.A=blood.m-blood.m0.A
blood.m
blood.m0.A
blood.m1.A
blood.m0.D=estimate.m0(bloodPvalues.D)
blood.m1.D=blood.m-blood.m0.D
blood.m0.D
blood.m1.D

#Convert p-values to q-values.

bloodQvalues.A=jabes.q(bloodPvalues.A)
bloodQvalues.D=jabes.q(bloodPvalues.D)
#Find number of genes declared to be DE
#for various FDR thresholds.
fdrCuts=seq(0.01,.15,by=0.001)
bloodSigGene.A=cbind(fdrCuts,NumberOfGenes=apply(outer(bloodQvalues.A,fdrCuts,"<="),2,sum))
bloodSigGene.A
write.csv(bloodSigGene.A,file="bloodSigGene-A.csv")
bloodSigGene.D=cbind(fdrCuts,NumberOfGenes=apply(outer(bloodQvalues.D,fdrCuts,"<="),2,sum))
bloodSigGene.D
write.csv(bloodSigGene.D,file="bloodSigGene-D.csv")
#bloodPCs=prcomp(t(bloodMA$M),center=T,scale.=T,retx=T)
#plot(bloodPCs$x[,1],bloodPCs$x[,2],pch=16,col=rep(1:2,each=4))

###########


ouc=ub.mix(puc,c(.5,1,10))
plot.ub.mix(puc,ouc)

otc=ub.mix(ptc,c(.5,1,10))
plot.ub.mix(ptc,otc)

otu=ub.mix(ptu,c(.7,2,5))
plot.ub.mix(ptu,otu)

ppdeuc=ppde(puc,ouc)
ppdetc=ppde(ptc,otc)
ppdetu=ppde(ptu,otu)
max(ppdetu)















#Examine position of genes declared to be DE at FDR=0.01
#in MA plot for slide 6.

plot(MA$A[,6],MA$M[,6],
     col=1+(qvals<=0.01),
     pch=3+14*(qvals<=0.01),cex=.6)

#Fit uniform-beta mixture model.

out=ub.mix(p)
out

plot.ub.mix(p,out)

#Change starting values and refit
#uniform-beta mixture model.

out=ub.mix(p,c(.5,1,10))
out

plot.ub.mix(p,out)

#Compute estimated ppde for each gene.

eppde=ppde(p,out)

plot(p,eppde,xlab="p-value",
             ylab="Estimated Posterior Probability of DE",
             cex.lab=1.5)

ppdecuts=seq(.6,.95,by=0.05)
cbind(ppdecuts,NumberOfGenes=apply(outer(eppde,ppdecuts,">="),2,sum))
