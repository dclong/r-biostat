#true TC
trueTC = neuronEBayesFitDifftc$coef[!geneDropBool,1] - bloodEBayesFitA$coef[,1]
#based on simple calculation we know var(Cy5-Cy3+Tt-Tc)=sigma^2/4, and the same for others
neuronTC.var = neuronEBayesFitDifftc$s2.post[!geneDropBool]/4
bloodCy5Cy3.var = bloodEBayesFitA$s2.post/4
trueTC.var = neuronTC.var + bloodCy5Cy3.var
neuronTC.df = neuronEBayesFitDifftc$df.prior + neuronEBayesFitDifftc$df.residual
neuronTC.df = neuronTC.df[!geneDropBool]
bloodCy5Cy3.df = bloodEBayesFitA$df.prior + bloodEBayesFitA$df.residual
trueTC.df = apply(cbind(neuronTC.var,bloodCy5Cy3.var,neuronTC.df,bloodCy5Cy3.df),1,
                  function(x){df.ws(x[1:2],c(1,1),x[3:4])})
trueTC.tvals = trueTC/sqrt(trueTC.var)
trueTC.pvalues = pt(abs(trueTC.tvals),trueTC.df,lower.tail=F)*2
hist(trueTC.pvalues,col=4)
box()
dev.copy2pdf(file="trueTCpvals.pdf")
jabes.q(trueTC.pvalues)->trueTC.qvals
fdrCuts=seq(0.01,0.15,by=0.001)
trueTC.SigGene=cbind(fdrCuts,NumberOfGenes=apply(outer(trueTC.qvals,fdrCuts,"<="),2,sum))
trueTC.SigGene  
write.csv(trueTC.SigGene,file="trueTC-sigGene.csv")
                  
#true UC
trueUC = neuronEBayesFitDiffuc$coef[!geneDropBool,1] - bloodEBayesFitA$coef[,1]
#based on simple calculation we know var(Cy5-Cy3+Tt-UC)=sigma^2/4, and the same for others
neuronUC.var = neuronEBayesFitDiffuc$s2.post[!geneDropBool]/4
bloodCy5Cy3.var = bloodEBayesFitA$s2.post/4
trueUC.var = neuronUC.var + bloodCy5Cy3.var
neuronUC.df = neuronEBayesFitDiffuc$df.prior + neuronEBayesFitDiffuc$df.residual
neuronUC.df = neuronUC.df[!geneDropBool]
bloodCy5Cy3.df = bloodEBayesFitA$df.prior + bloodEBayesFitA$df.residual
trueUC.df = apply(cbind(neuronUC.var,bloodCy5Cy3.var,neuronUC.df,bloodCy5Cy3.df),1,
                  function(x){df.ws(x[1:2],c(1,1),x[3:4])})
trueUC.tvals = trueUC/sqrt(trueUC.var)
trueUC.pvalues = pt(abs(trueUC.tvals),trueUC.df,lower.tail=F)*2
hist(trueUC.pvalues,col=4)
dev.copy2pdf(file="trueUCpvals.pdf")
jabes.q(trueUC.pvalues)->trueUC.qvals
fdrCuts=seq(0.01,0.15,by=0.001)
trueUC.SigGene=cbind(fdrCuts,NumberOfGenes=apply(outer(trueUC.qvals,fdrCuts,"<="),2,sum))
trueUC.SigGene          
write.csv(trueUC.SigGene,file="trueUC-sigGene.csv") 


#compare the original pvalues and the adjusted ones
neuronPtc.keep = neuronPtc[!geneDropBool]
plot(-log(neuronPtc.keep,10),-log(trueTC.pvalues,10),main="Adjusted Pvalues VS Unadjusted\nTreated VS Carrier")
dev.copy2pdf(file="adjustedP-tc.pdf")
neuronPuc.keep = neuronPuc[!geneDropBool]
plot(-log(neuronPuc.keep,10),-log(trueUC.pvalues,10),main="Adjusted Pvalues VS Unadjusted\nUntreated VS Carrier")
dev.copy2pdf(file="adjustedP-uc.pdf")

#####################################################################
#compare the t statistics for c VS u for the two experiments
fdim=cbind(range(neuronEBayesFitDiffuc$t[!geneDropBool]),range(bloodEBayesFitD$t))
fdim.lower=min(fdim[1,])
fdim.lower
fdim.upper=max(fdim[2,])
fdim.upper
flim = c(fdim.lower,fdim.upper)
plot(neuronEBayesFitDiffuc$t[!geneDropBool],bloodEBayesFitD$t,cex=0.3,asp=1,
xlab="Neuron (Uncorrected)",ylab="Blood",main="T Statistics for Carrier VS Untreated")
corBloodNeuron.p=cor(neuronEBayesFitDiffuc$t[!geneDropBool],bloodEBayesFitD$t,method="pearson")
corBloodNeuron.s=cor(neuronEBayesFitDiffuc$t[!geneDropBool],bloodEBayesFitD$t,method="spearman")
mytext(paste("cor.p = ",round(corBloodNeuron.p,4),"\n","cor.s = ", round(corBloodNeuron.s,4),sep=""),border=T)
text(x=-10,y=7.99,labels="cor.p=-0.0823\ncor.s=-0.0834",adj=NULL,col="blue")


plot(trueUC.tvals,bloodEBayesFitD$t,cex=0.3,asp=1,
     xlab="Neuron (Corrected)",ylab="Blood",main="T Statistics for Carrier VS Untreated")
corBloodTrue.p=cor(trueUC.tvals,bloodEBayesFitD$t,method="pearson")
corBloodTrue.p
corBloodTrue.s=cor(trueUC.tvals,bloodEBayesFitD$t,method="spearman")
corTrueBlood.s
mytext(paste("cor.p = ",round(corBloodTrue.p,4),"\n","cor.s = ", round(corBloodTrue.s,4),sep=""),border=T)
text(x=-12,y=8,labels="cor.p=-0.0425\ncor.s=-0.0443",adj=NULL,col="blue")





plot(neuronEBayesFitDiffuc$t[!geneDropBool],trueUC.tvals,cex=0.3,asp=1,
     xlab="Neuron (Uncorrected)",ylab="Neuron (Corrected)",main="T Statistics for Carrier VS Untreated")
corAdjustedUnadjusted.p=cor(neuronEBayesFitDiffuc$t[!geneDropBool],trueUC.tvals,method="pearson")
corAdjustedUnadjusted.s=cor(neuronEBayesFitDiffuc$t[!geneDropBool],trueUC.tvals,method="spearman")
mytext(paste("cor.p = ",round(corAdjustedUnadjusted.p,4),"\n","cor.s = ", round(corAdjustedUnadjusted.s,4),sep=""),border=T)
text(x=-6.5,y=7,labels="cor.p=0.8509\ncor.s=0.8498",adj=NULL,col="blue")



plot(neuronEBayesFitDifftc$t[!geneDropBool],trueTC.tvals,cex=0.3,asp=1,
     xlab="Neuron (Uncorrected)",ylab="Neuron (Corrected)",main="T Statistics for Carrier VS Treated")
corAdjustedUnadjusted.tc.p=cor(neuronEBayesFitDifftc$t[!geneDropBool],trueTC.tvals,method="pearson")
corAdjustedUnadjusted.tc.s=cor(neuronEBayesFitDifftc$t[!geneDropBool],trueTC.tvals,method="spearman")

mytext(paste("cor.p = ",round(corAdjustedUnadjusted.tc.p,4),"\n","cor.s = ", round(corAdjustedUnadjusted.tc.s,4),sep=""),border=T)
text(x=-8,y=7.8,labels="cor.p=0.8506\ncor.s=0.8477",adj=NULL,col="blue")




