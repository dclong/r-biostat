#' make histograms of pvalues afte switching pairs of indeces
checkLabels = function(index){
  if(is.vector(index)){
    index = t(index)
  }
  folder = "plots/fit1/design"
  n = nrow(index)
  for(i in 1:n){
    folder = paste(folder,index[i,1],index[i,2],sep="")
  }
  phplot(switchRows(index),folder)
}
