#read in data from the csv document
d=read.csv("transcript_read_count_v2.csv")
head(d)

#Remove first two rows and columns for non-fractional counts.

d=d[-(1:2),-(1+seq(3,25,by=2))]
head(d)
for(i in 2:14){
 d[,i]=round(as.numeric(as.character(d[,i])),0)
}
head(d)


#Remove transcripts with all zero counts.

d=d[apply(d[,-(1:2)],1,sum)>0,]
dim(d)

#Save transcript names and transcript lengths

tn=d[,1]
tl=d[,2]

#Create matrix of counts whose row names are the transcript names.

counts=d[,-(1:2)]
names(counts)=c("14SAM1","14SAM2","14LM1","14LM2","Cp1","Cp2","L11","L12",
"Pro1","Pro2","Tran1","Tran2")
row.names(counts)=tn
#convert data frame to matrix
counts = as.matrix(counts)
head(counts,30)

#Get .75 quantiles of count distributions for each sample.

adjust=apply(counts,2,quantile,.75)
adjust
la=log(adjust)

#create a treatment factor
trt = factor(c("14SAM","14SAM","14LM","14LM","Cp","Cp","L1","L1","Pro","Pro","Tran","Tran"))


testAllGene(counts,trt,la,comp.pairwise)->test.pairwise

test.pairwise[,grep('-Pval',colnames(test.pairwise),value=T)]->test.pvals
hist(test.pvals[,'O-Pval'],col="blue",xlab="pvalues",main="Pvalues for Testing the Whole Model")
dev.copy2pdf(file="plots/whole-pval.pdf")
hist(test.pvals[,"14LM-14SAM-Pval"],col="blue",xlab="pvalues",main="Pvalues for 14SAM VS 14LM")
dev.copy2pdf(file="plots/14LM-14SAM-pval.pdf")

local(
for(i in 1:ncol(test.pvals)){
  compare = colnames(test.pvals)[i]
  compare.title = gsub("-Pval","",compare,fixed=T)
  compare.title = gsub("-","VS",compare.title,fixed=T)
  title = paste("Pvalues for",compare.title)
  hist(test.pvals[,i],col="blue",xlab="pvalues",main=title)
  path = paste('plots/',compare,".pdf",sep="")
  dev.copy2pdf(file=path)
}
)


pvals.pairwise = out.pc$pval
dim(pvals.pairwise)
#check whether these pvalues are reasonable
counts[pvals.pairwise[,1]<=0.491&pvals.pairwise[,1]>0.4905,]

zcount=apply(counts==0,1,sum)
hist(zcount)
table(zcount)
boxplot(pvals.pairwise[,1]~zcount)

min(pvals.pairwise[,1][zcount==11])
min(pvals.pairwise[,1][zcount==10])
tcounts=rowSums(counts)
hist(tcounts[tcounts<1000])
hist(pvals.pairwise[,1][tcounts<13])
min(pvals.pairwise[,1][tcounts<13])
counts[tcounts<13&pvals.pairwise[,1]<0.004466,]
min(pvals.pairwise[,1][tcounts<10])
counts[tcounts<10&pvals.pairwise[,1]<0.01,]

p=p[zcount<11]
P.less11=pvals.pairwise[,-1][zcount<11,]
B=B[zcount<11,]
counts=counts[zcount<11,]
tl=tl[zcount<11]
tn=tn[zcount<11]
#check whether there's any NA value
sum(is.na(P.less11))
#histogram of pvals, the matrix containing pvales must have column names
hist.pval(pvals.pairwise,'plots')
#calculate q values
qvals.pairwise = apply(pvals.pairwise,2,qvalues.nettleton,chip='r')
#save comparison plots of p and q values 
save.plot.pq(pvals.pairwise,qvals.pairwise,'plots')

#calculate number of significant genes
fdrcuts=seq(.01,0.15,by=0.01)

significant(fdrcuts,q=qvals.pairwise)->sig.gene


#calculate log2 folder change
cont.mat=matrix(c(
        0,-1,0,0,0,0,
        0,0,-1,0,0,0,
        0,0,0,-1,0,0,
        0,0,0,0,-1,0,
        0,0,0,0,0,-1,
        0,1,-1,0,0,0,
        0,1,0,-1,0,0,
        0,1,0,0,-1,0,
        0,1,0,0,0,-1,
        0,0,1,-1,0,0,
        0,0,1,0,-1,0,
        0,0,1,0,0,-1,
        0,0,0,1,-1,0,
        0,0,0,1,0,-1,
        0,0,0,0,1,-1),byrow=T,nrow=15)

cont.mat
non.log2fc=out.pc$coef%*%t(cont.mat)
log2fc=non.log2fc*log2(exp(1))

#----- write results into csv document --------
#pvalues
write.csv(out.pc$pval,file="Conclusions/pvals.csv")
#qvalues
write.csv(qvals.pairwise,file="Conclusions/qvals.csv")
#log2 folder change
write.csv(log2fc,file="Conclusions/log2fc.csv")
#number of significant genes
write.csv(sig.gene,file="Conclusions/sig.csv")

#I don't know what the following code is for
geo.mean.mmr=exp(mean(log(totals/1000000)))

m=exp(cbind(B[,1],B[,-1]+B[,1])+mean(la))/(tl/1000)/geo.mean.mmr

results=data.frame(tn,tl,counts,m,p,P,qvals,l2fc)






