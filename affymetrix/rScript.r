.Begining=function(){}
#read in data from CEL files
.ReadData=function(){}#for fast reference use, never run it
dir("CEL",full.names=T)->files.cel
files.cel
ReadAffy(filenames=files.cel)->data
#attributes of data   
attributes(data)
#sample names
sampleNames(data)
#change sample names
sampleNames(data)->sample.names
gsub('^.{4}_','',sample.names)->sample.names
sample.names
gsub('_.*$','',sample.names)->sample.names
sample.names
sample.names[1:6] = as.vector(t(outer(c('R1','R2'),1:3,FUN=paste,sep="")))
sample.names
sample.names->sampleNames(data)
sampleNames(data)
#information about how to match the CEL files to treatment information
phenoData(data)

#PM density for all gene chips 
.PMDensity=function(){}
cols = c("blue","red","green")
rep(cols,each=3)->cols
hist(data,main="PM Densities",col=cols)
text(x=12.7359694427494,y=0.729562397328298,labels="Red: Phenotype 2",    adj=NULL,    col="blue")
text(x=12.7359694427494,y=0.629562397328298,labels="Red: Phenotype 2",    adj=NULL,    col="red")
text(x=12.7359694427494,y=0.529562397328298,labels="Green: Vec Contr",    adj=NULL,    col="Green")
dev.copy2pdf(file="plots/pmDen.pdf")

#side-by-side boxplot of log2 PM
boxplot(data,col=cols)
dev.copy2pdf(file="plots/boxplot-log2PM.pdf")
#image of the first gene chip
image(data[,1])

#gene names
geneNames(data)->gene.names
length(gene.names)
.PMMMMatches=function(){}
#pm data for the first gene
pm(data,gene.names[1])->pm.gene1
mm(data,gene.names[1])->mm.gene1
dim(pm.gene1)
#pm vs mm for gene 1 chip 1
plot.pmmm(1,1,data,folder="Plots",save=T)
plot.pmmm(1,2,data,folder="Plots",save=T)
plot.pmmm(1,3,data,folder="Plots",save=T)
#proportion of pm<mm for gene 1 chip 1
sapply(1:9,rate.pmmm,gene=1,data=data)
rate.pmmm(1,1,data)
rate.pmmm(1,2,data)
rate.pmmm(1,3,data)
rate.pmmm(1,4,data)
mean(pm(data)<mm(data))->rate
rate

#MA plot of the data
.MAPlot=function(){}
library(geneplotter)
MAplot(data[,1:9],pairs=T,plot.method="smoothScatter")
dev.copy2pdf(file="Plots/maplot-overall.pdf")
MAplot(data[,1:3],pairs=T,plot.method="smoothScatter")
dev.copy2pdf(file="Plots/maplot-R1.pdf")
MAplot(data[,1],pairs=F,plot.method="smoothScatter")
dev.copy2pdf(file="Plots/matplot-chip1.pdf")
#create RMA data and expression data
data.rma=rma(data)
data.rma
data.expr=exprs(data.rma)
dim(data.expr)
head(data.expr)
boxplot(as.data.frame(data.expr),col=cols,las=3)
dev.copy2pdf(file="boxplot-expr.pdf")

#---Because of the specail design of this experiment, it's not valid to analyze it in the usualy way. Here I use two ways to analyze the data.

.End=function(){}

