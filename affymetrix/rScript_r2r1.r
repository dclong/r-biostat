.Begining=function(){}
#-Method I: split data and fit model
.FitModelr2r1=function(){}
#extract data for comparing vector control and phenotype 1 of ring
data.expr.r2r1 = data.expr[,-(7:9)]
head(data.expr.r2r1)
data.expr.r2r1 = data.expr.r2r1[,4:6] - data.expr.r2r1[,1:3]
head(data.expr.r2r1)
colnames(data.expr.r2r1)=paste('D',1:3,sep="")
head(data.expr.r2r1)
#build the design matrix
matrix(1,nrow=3,ncol=1)->design.r2r1
colnames(design.r2r1)=c('R2_R1')
design.r2r1
#fit the model to the data expression for vector control and phenotype 1
fit.r2r1 = lmFit(data.expr.r2r1,design.r2r1)
contrast.r2r1 = makeContrasts('R2_R1',levels=design.r2r1)
fit.r2r1.r2_r1 = contrasts.fit(fit.r2r1,contrast.r2r1)
fit.r2r1.r2_r1.ebayes = eBayes(fit.r2r1.r2_r1)
fit.r2r1.r2_r1.ebayes$df.prior
fit.r2r1.r2_r1.ebayes$s2.prior
#compare the variances
plot.sd(var.or=fit.r2r1.r2_r1.ebayes$sigma,var.ad=fit.r2r1.r2_r1.ebayes$s2.post,cex=0.3,xlab="Log(Original SD)",ylab="Log(Adjusted SD",asp=1,h=sqrt(fit.r2r1.r2_r1.ebayes$s2.prior),v=sqrt(fit.r2r1.r2_r1.ebayes$s2.prior),main="Phenotype 2 VS Phenotype 1")
dev.copy2pdf(file="plots/sds-r2r1.pdf")
#histogram of pvalues 
hist.p(fit.r2r1.r2_r1.ebayes$p.value,folder="plots",save=T)
dim(fit.r2r1.r2_r1.ebayes$p.value)
head(fit.r2r1.r2_r1.ebayes$p.value)
write.csv(fit.r2r1.r2_r1.ebayes$p.value,file="Conclusions/pvals-r2r1.csv")
#qvalues
qvals.r2r1 = q.benjamini(fit.r2r1.r2_r1.ebayes$p.value)
write.csv(qvals.r2r1,file="Conclusions/qvals-r2r1.csv")
#log2fc 
dim(fit.r2r1.r2_r1.ebayes$coef)
head(fit.r2r1.r2_r1.ebayes$coef)
write.csv(fit.r2r1.r2_r1.ebayes$coef,file="Conclusions/log2fc-r2r1.csv")
#significnat genes
.ConclusionModelVR2=function(){}
fdr = seq(0.01,0.15,by=0.01)
fdr
significant(fdr,fit.r2r1.r2_r1.ebayes$p.value,q.benjamini)->sig.modr2r1
sig.modr2r1
write.csv(sig.modr2r1,file="Conclusions/sig-mod-r2r1.csv")
.End=function(){}
