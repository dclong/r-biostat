.Begining=function(){}
#-Method I: split data and fit model
.FitModelvr2=function(){}
#extract data for comparing vector control and phenotype 1 of ring
data.expr.vr2 = data.expr[,-(1:3)]
head(data.expr.vr2)
#build the design matrix
factor.vr2 = as.factor(rep(c('R2','V'),each=3))
factor.vr2
model.matrix(~factor.vr2)->design.vr2
colnames(design.vr2)=c("Intercept",'V_R2')
design.vr2
#fit the model to the data expression for vector control and phenotype 1
fit.vr2 = lmFit(data.expr.vr2,design.vr2)
contrast.vr2 = makeContrasts('V_R2',levels=design.vr2)
fit.vr2.v_r2 = contrasts.fit(fit.vr2,contrast.vr2)
fit.vr2.v_r2.ebayes = eBayes(fit.vr2.v_r2)
fit.vr2.v_r2.ebayes$df.prior
fit.vr2.v_r2.ebayes$s2.prior
#compare the variances
plot.sd(var.or=fit.vr2.v_r2.ebayes$sigma,var.ad=fit.vr2.v_r2.ebayes$s2.post,cex=0.3,xlab="Log(Original SD)",ylab="Log(Adjusted SD",asp=1,h=sqrt(fit.vr2.v_r2.ebayes$s2.prior),v=sqrt(fit.vr2.v_r2.ebayes$s2.prior),main="Vector Control VS Phenotype 2")
dev.copy2pdf(file="plots/sds-vr2.pdf")
#histogram of pvalues 
hist.p(fit.vr2.v_r2.ebayes$p.value,folder="Plots",save=T)
#write pvalues to files
dim(fit.vr2.v_r2.ebayes$p.value)
head(fit.vr2.v_r2.ebayes$p.value)
write.csv(fit.vr2.v_r2.ebayes$p.value,file="Conclusions/pvals-mod-vr2.csv")
qvals.vr2 = q.benjamini(fit.vr2.v_r2.ebayes$p.value)
dim(qvals.vr2)
head(qvals.vr2)
write.csv(qvals.vr2,file="Conclusions/qvals-mod-vr2.csv")
#log2fc
dim(fit.vr2.v_r2.ebayes$coef)
head(fit.vr2.v_r2.ebayes$coef)
write.csv(fit.vr2.v_r2.ebayes$coef,file="Conclusions/log2fc-mod-vr2.csv")

#significnat genes
.ConclusionModelVR2=function(){}
fdr = seq(0.01,0.15,by=0.01)
fdr
significant(fdr,fit.vr2.v_r2.ebayes$p.value,q.benjamini)->sig.modvr2
write.csv(sig.modvr2,file="Conclusions/sig-mod-vr2.csv")
.End=function(){}
