.Begining=function(){}
#-Method I: split data and fit model
.FitModelVR1=function(){}
#extract data for comparing vector control and phenotype 1 of ring
data.expr.vr1 = data.expr[,-(4:6)]
head(data.expr.vr1)
#build the design matrix
factor.vr1 = as.factor(rep(c('R1','V'),each=3))
factor.vr1
model.matrix(~factor.vr1)->design.vr1
colnames(design.vr1)=c("Intercept",'V_R1')
design.vr1
#fit the model to the data expression for vector control and phenotype 1
fit.vr1 = lmFit(data.expr.vr1,design.vr1)
contrast.vr1 = makeContrasts('V_R1',levels=design.vr1)
fit.vr1.v_r1 = contrasts.fit(fit.vr1,contrast.vr1)
fit.vr1.v_r1.ebayes = eBayes(fit.vr1.v_r1)
fit.vr1.v_r1.ebayes$df.prior
fit.vr1.v_r1.ebayes$s2.prior
#compare the variances
plot.sd(var.or=fit.vr1.v_r1.ebayes$sigma,var.ad=fit.vr1.v_r1.ebayes$s2.post,cex=0.3,xlab="Log(Original SD)",ylab="Log(Adjusted SD",main="Vector Control VS Phenotype 1",asp=1,h=sqrt(fit.vr1.v_r1.ebayes$s2.prior),v=sqrt(fit.vr1.v_r1.ebayes$s2.prior))
dev.copy2pdf(file="plots/sds.pdf")
#histogram of pvalues 
hist.p(fit.vr1.v_r1.ebayes$p.value,folder="plots",save=T)
dim(fit.vr1.v_r1.ebayes$p.value)
write.csv(fit.vr1.v_r1.ebayes$p.value,file="Conclusions/pvals-mod-vr1.csv")
dim(fit.vr1.v_r1.ebayes$coef)
head(fit.vr1.v_r1.ebayes$coef)
write.csv(fit.vr1.v_r1.ebayes$coef,file="Conclusions/log2fc-mod-vr1.csv")
#q values
qval.vr1 = q.benjamini(fit.vr1.v_r1.ebayes$p.value)
dim(qval.vr1)
head(qval.vr1)
write.csv(qval.vr1,file="conclusions/qvals-mod-vr1.csv")
#significnat genes
.ConclusionModelVR1=function(){}
fdr = seq(0.01,0.15,by=0.01)
fdr
significant(fdr,fit.vr1.v_r1.ebayes$p.value,q.benjamini)->sig.modvr1
write.csv(sig.modvr1,file="Conclusions/sig_mod_vr1.csv")
.End=function(){}