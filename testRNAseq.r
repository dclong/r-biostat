#' Tests for RNA Sequence Data

#' Function \code{testRNAseq} performs the required tests for RNA sequence data. Function \code{updateRNAseq} updates the output of function \ocde{testRNAseq} if more tests are to be performed. Surely you can you function \code{testRNAseq} to performs the new tests, but using function \code{updateRNAseq}, you can save some time and you don't have to create a new object to save the output. 
#' @param test.out the output of function \code{testRNAseq}.
#' @param x.full the design matrix of the full model.
#' @param x.red a list of design matrices corresponding to reduced models. You can names these design matrices so that the output is more readable. It's not required when function \code{updateRNAseq} is called.
#' @param data a data matrix with each row representing a response vector for a gene.
#' @param log.75q a numerical vector, which contains the logarithm of 75% quantile of the counts for genes under each treatment combination. It is used as offset in \code{glm.fit}.
#' @param cont.mat a matrix with columns containing reduced contrasts, i.e. with apropriate column(s) dropped from the full contrasts matrix. If specified, it must have the same number of columns as the length of x.red, and the order of contrast (in the columns) must be consistent with the order of design matrices corresponding to reduced models in \code{x.red}.
#' @param q.mathod the function used to calculate the q values. If it's specified when used in function \code{updateRNAseq}, the original \code{q.method} will be overwritten and and the qvalues and number of significant genes will be re-calculated.
#' @param fdr the level of false discover rates; vector supported. If it's specified when used in function \code{updateRNAseq}, the orginal \code{fdr} will be overwritten and the number of significant genes will be re-calculated. 
#' @param ... more arguments that can be passed to function \code{q.method}.
#' @export testRNAseq updateRNAseq 
updateRNAseq = function(test.out,x.red,cont.mat,q.method,fdr,...){
  if(missing(x.red)){
    x.red = list()
  }
  if(is.vector(x.red,'numeric')||is.matrix(x.red)){
      x.red = list(x.red)
  }else if(!is.list(x.red)){
      stop("argument x.red should be a list of matrices.")
  }
  test.out$x.reduced = c(test.out$x.reduced,x.red)
  red.num = length(x.red)
  df.contrast = matrix(0,nrow=test.out$gene.number,ncol=red.num)
  fstat = matrix(0,nrow=test.out$gene.number,ncol=red.num)
  pval = matrix(0,nrow=test.out$gene.number,ncol=red.num)
  log2FC = NULL
  qval = NULL
  sig.gene = NULL
  for(i in 1:gene.number){
    j = 1
    while(j<=red.num){
      #fit a reduced model
      red.model = glm.fit(x=x.red[[j]],y=test.out$data[i,],
                          family=poisson(link=log),offset=log.75q)
      #comapred this reduced model with the full model
      ndf.tmp = red.model$df.residual - test.out$df.residual
      fstat.tmp = (red.model$deviance - test.out$deviance)/ndf.tmp/test.out$dispersion
      pval.tmp = pf(q=fstat.tmp,df1=ndf.tmp,df2=test.out$df.residual,lower.tail=FALSE)
      #record results
      df.contrast[i,j] = ndf.tmp
      fstat[i,j] = fstat.tmp
      pval[i,j] = pval.tmp
      j = j + 1
    }
  }
  #headers
  header = names(x.red)
  #------------------------
  if(!missing(cont.mat)){
    log2FC = test.out$coefficients%*%cont.mat*log2(exp(1))
    colnames(log2FC) = header
  }
  if(missing(q.method)&&missing(...)){
    funList = test.out$q.method
    if(!is.null(funList$method)){
      mode(funList) = "call"
      qval = eval(funList)
      colnames(qval) = header
    }
  }else{
    if(!missing(q.method)){
      test.out$q.method$method = q.method
    }
    if(!missing(...)){
      test.out$q.method = list(method=test.out$q.method$method,...)
    }
    funList = test.out$q.method
    if(!is.null(funList$method)){
      #update test.out$qval
      funList1 = list(funList,p=test.out$pval)
      mode(funList1) = "call"
      test.out$qval = eval(funList1)
      colnames(test.out$qval) = colnames(test.out$pval)
      test.out$sig.gene = significant(fdr=test.out$fdr,q=test.out$qval)
      colnames(test.out$sig.gene) = colnames(test.out$pval)
      #calculate new qval
      funList2 = list(funList,p=pval)
      model(funList2) = "call"
      qval = eval(funList2)
      colnames(qval) = header
    }else{
      test.out$qval = NULL
      test.out$sig.gene = NULL
    }
  }
  if(missing(fdr)){
    sig.gene = significant(fdr=test.out$fdr,q=qval)
    colnames(sig.gene) = header
  }else{#have to updatea all sig.gene
    test.out$fdr = fdr
    test.out$sig.gene = significant(fdr=fdr,q=test.out$qval)
    colnames(test.out$sig.gene) = colnames(test.out$pval)
    sig.gene = significant(fdr=fdr,q=qval)
    colnames(sig.gene) = header
  }
  colnames(df.contrasts) = header
  colnames(fstat) = header
  colnames(pval) = header
  #update the object test.out 
  test.out$df.contrast = cbind(test.out$df.contrast,df.contrast)
  test.out$fstat = cbind(test.out$fstat,fstat)
  test.out$pval = cbind(test.out$pval,pval)
  test.out$log2FC = cbind(test.out$log2FC,log2FC)
  test.out$qval = cbind(test.out$qval,qval)
  test.out$sig.gene = cbind(test.out$sig.gene,sig.gene)
  return(test.out)
}

testRNAseq = function(data,x.full,log.75q,x.red,cont.mat,q.method,fdr,...){
  if(!is.matrix(data)){
    data = as.matrix(data)
    warning("argument data is converted to a matrix.")
  }
  if(is.vector(x.red,'numeric')||is.matrix(x.red)){
      x.red = list(x.red)
  }else if(!is.list(x.red)){
      stop("argument x.red should be a list of matrices.")
  }
  gene.num = nrow(data)
  red.num = length(x.red)
  para.num = ncol(x.full)
  #initial variables for storing final results
  dispersion = rep(0,gene.num)
  deviance = rep(0,gene.num)
  df.residual = rep(0,gene.num)
  coefficients = matrix(0,nrow=gene.num,ncol=para.num)
  df.contrast = matrix(0,nrow=gene.num,ncol=red.num)
  fstat = matrix(0,nrow=gene.num,ncol=red.num)
  pval = matrix(0,nrow=gene.num,ncol=red.num)
  log2FC = NULL
  qval = NULL
  sig.gene = NULL
  #gene names
  gene.names = rownames(data)
  rownames(coefficients) = gene.names
  rownames(df.contrast) = gene.names
  rownames(fstat) = gene.names
  rownames(pval) = gene.names
  names(dispersion) = gene.names
  names(deviance) = gene.names
  names(df.residual) = gene.names
  #fit each gene
  for(i in 1:gene.num){
    testOneGene(x.full=x.full,y=data[i,],log.75q=log.75q,x.red=x.red)->one.gene.out
    deviance[i] = one.gene.out$deviance
    coefficients[i,] = one.gene.out$coefficients
    dispersion[i] = one.gene.out$dispersion
    df.residual[i] = one.gene.out$df.residual
    df.contrast[i,] = one.gene.out$df.contrast
    fstat[i,] = one.gene.out$fstat
    pval[i,] = one.gene.out$pval
  }
  #extract column names
  header.red = names(one.gene.out$pval)
  header.coef = names(one.gene.out$coefficients)
  if(!missing(cont.mat)){
    log2FC = coefficients%*%cont.mat*log2(exp(1))
    colnames(log2FC) = header.red
  }
  if(!missing(q.method)){
    qval = q.method(pval,...)
    colnames(qval) = header.red
  }else{
    q.method = NULL
  }
  if(!missing(fdr)){
      sig.gene = significant(fdr=fdr,q=qval)
      colnames(sig.gene) = header.red
  }else{
      fdr = NULL
  }
  colnames(coefficients) = header.coef
  colnames(df.contrast) = header.red
  colnames(fstat) = header.red
  colnames(pval) = header.red
  list(x.full=x.full,x.reduced=x.red,fdr=fdr,q.method=list(method=q.method,...),
       data=data,gene.number=gene.num,
       offset=log.75q,coefficients=coefficients,
       deviance=deviance,dispersion=dispersion,df.residual=df.residual,
       df.contrast=df.contrast,fstat=fstat,pval=pval,
        log2FC=log2FC,qval=qval,sig.gene=sig.gene)
}

testOneGene = function(x.full,y,log.75q,x.red){
  full.model = glm.fit(x=x.full,y=y,family=poisson(link=log),offset=log.75q)
  #initial variables for storing results
  ndf = NULL
  ddf = full.model$df.residual
  fdev = full.model$deviance
  fcoef = full.model$coefficients
  phihat = full.model$deviance/ddf
  if(phihat<1){
      phihat = 1
  }
  fstat = NULL
  pval = NULL
  #--------- do f tests ------------
  #get the number of reduced models
  redMod.num = length(x.red)
  #initial variables for storing results
  ndf = rep(0,redMod.num)
  fstat = ndf
  pval = ndf
  for(i in 1:redMod.num){
    #extract the design matrix
    dm.red = x.red[[i]]
    #fit a reduced model
    red.model = glm.fit(x=dm.red,y=y,family=poisson(link=log),offset=log.75q)
    #comapred the two models
    ndf.tmp = red.model$df.residual - full.model$df.residual
    fstat.tmp = (red.model$deviance - full.model$deviance)/ndf.tmp/phihat
    pval.tmp = pf(q=fstat.tmp,df1=ndf.tmp,df2=ddf,lower.tail=FALSE)
    #record results
    ndf[i] = ndf.tmp
    fstat[i] = fstat.tmp
    pval[i] = pval.tmp
  }
  header.red = names(x.red)
  names(ndf) = header.red
  names(fstat) = header.red
  names(pval) = header.red
  list(coefficients=fcoef,deviance=fdev,dispersion=phihat,
       df.residual=ddf,df.contrast=ndf,fstat=fstat,pval=pval)
}




