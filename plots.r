#' Plots the original and adjusted values to compare them. For example you can use this function to compare the variances estimated using linear regression and the adjusted varainces using Bayes method, or you can compare pvalues and qvalues, and so on. Function \code{plot.pmmm} plot the perfect matches and mismatches in different colors and symbols to compare them. 
#' @param orignal a numerical vector. 
#' @param adjusted another numerical vector. 
#' @param p a vector of pvalues.
#' @param q a vector of qvalues.
#' @param ctype either "xy","yx" or "yy" which indicate whether to treat the two variables as x variable or y variable.
#' @param folder the path of the folder in which the plots are to be saved. 
#' @param var.or original variances.
#' @param var.ad adjusted variances.
#' @param ... more arguments that can be passed to function \code{plot}.

plot.pmmm = function(gene,chip,data,folder=".",save=FALSE,...){
  gene.names = geneNames(data)
  pm(data,gene.names[gene])[,chip]->pm.gene.chip
  mm(data,gene.names[gene])[,chip]->mm.gene.chip
  path = paste(folder,'/pm_mm_',gene,'_',chip,".pdf",sep="")
  data.length = length(pm.gene.chip)
  pch = rep(c('p','m'),each=data.length)
  col = rep(c('red','blue'),each=data.length)
  main=paste("PM VS MM for Gene",gene,"Chip",chip)
  plot.comparison(original=pm.gene.chip,adjusted=mm.gene.chip,ctype="yy",path=path,save=save,xlab="Index",ylab="Matches",main=main,pch=pch,col=col,...)
}

plot.comparison=function(original,adjusted,scale.fun,ctype,path,save=FALSE,h,v,...)
{
  if(!missing(scale.fun)){
      original = scale.fun(original)
      adjusted = scale.fun(adjusted)
      if(!missing(h)&&!is.null(h)){
          h = scale.fun(h)
      }
      if(!missing(v)&&!is.null(v)){
          v = scale.fun(v)
      }
  }
  if(ctype=="xy"){
    plot(original,adjusted,...)
    xlower = min(original)*10
    if(xlower==-Inf){
      xlower = -1e9
    }
    xupper = max(adjusted)*10
    if(xupper==Inf){
      xupper = 1e9
    }
    lines(c(xlower,xupper),c(xlower,xupper),col=2)
    if(!missing(h)){
        abline(h=h,col="blue")
    }
    if(!missing(v)){
        abline(v=v,col="blue")
    }
    if(save){
      dev.copy2pdf(file=path)
      cat('The plot is saved to the following file:\n',path,'\n')
    }
    return()
  }
  if(ctype=="yx"){
    return(plot.comparison(original=adjusted,adjusted=original,ctype="xy",path=path,save=save,...))
  }
  data.length = length(original)
  x = 1:data.length
  x = c(x,x)
  y = c(original,adjusted)
  plot(x,y,...)
  if(save){
    dev.copy2pdf(file=path)
  }
}

plot.pq = function(p,q,folder=".",save=FALSE,...){
  if(is.vector(p,'numeric')){
    p = as.matrix(p)
  }
  if(is.vector(q,'numeric')){
    q = as.matrix(q)
  }
  col.num = ncol(p)
  header = colnames(p)
  for(i in 1:col.num){
    title = header[i]
    path = paste(folder,"/pvsq-",title,".pdf",sep="")
    plot.comparison(original=-log10(p),adjusted=-log10(q),ctype="xy",path=path,save=save,main=title,
                  xlab="-log10(Pvalues)",ylab="-log10(Qvalues)",cex=0.4,...)
  }
}

plot.var=function(var.or,var.ad,scale.fun=log,path=".",save=FALSE,sd.or,sd.ad,...){
    if(!missing(sd.or)){
        var.or = sd.or^2
    }
    if(!missing(sd.ad)){
        var.ad = sd.ad^2
    }
        plot.comparison(original=var.or,adjusted=var.ad,scale.fun=scale.fun,ctype="xy",path=path,save=save,...)
}

plot.sd = function(sd.or,sd.ad,scale.fun=log,path=".",save=FALSE,var.or,var.ad,...){
    if(!missing(var.or)){
        sd.or = sqrt(var.or)
    }
    if(!missing(var.ad)){
        sd.ad = sqrt(var.ad)
    }
    plot.comparison(original=sd.or,adjusted=sd.ad,scale.fun=scale.fun,ctype="xy",path=path,save=save,...)
}

hist.p = function(p,folder=".",save=FALSE,...){
  if(is.vector(p,'numeric')){
    p = as.matrix(p)
  }
  test.num = ncol(p)
  p.names = colnames(p)
  for(i in 1:test.num){
    title = p.names[i]
    hist(p[,i],col="blue",xlab="Pvalues",main=paste("Pvalues for",title),...)
    box()
    if(save){
        file = paste(folder,"/phist-",title,".pdf",sep="")
        dev.copy2pdf(file=file)
        cat('The plot is saved to the following file:\n',file,'\n')
    }
  }
}
