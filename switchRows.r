
switchRows = function(rowIndex){
  result = design.matrix
  n = nrow(rowIndex)
  for(i in 1:n){
    result[rowIndex[i,1],] = design.matrix[rowIndex[i,2],]
    result[rowIndex[i,2],] = design.matrix[rowIndex[i,1],]
  }
  result
}