#' residuals from fitting a complete randomized model
checkLabels.res = function(dropIndex,data){
  design.matrix%*%solve(t(design.matrix)%*%design.matrix)%*%t(design.matrix)->H
  type = type[-dropIndex]
  n = nrow(data)
  result=matrix(0,nrow=n,ncol=length(dropIndex))
  for(i in 1:n){
    lm(data[i,-dropIndex]~type)->lmout
    res = lmout$res
    sigma = sqrt(sum(res^2)/(length(res)-2))
    #calculate fitting errors
    res=data[i,dropIndex]-design.matrix[dropIndex,]%*%lmout$coef
    res=res/sigma/sqrt(1-diag(H)[dropIndex])
    result[i,] = res
  }
  result
}