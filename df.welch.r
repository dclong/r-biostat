#' Function \code{df.welch} perfoms the Welch-Satterthwaite approximation to get approximate degrees of freedoms. 
df.welch = function(var,coef,df){
  var*coef->temp
  sum(temp)^2/sum(temp^2/df)
}