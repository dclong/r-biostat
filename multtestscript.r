#
#Read the example p-values discussed in the notes on multiple testing.
#The function "scan" is useful form reading a text file of numbers into
#a vector.
#

p=scan("http://www.public.iastate.edu/~dnett/microarray/classexamplep.txt")

#
#Make a histogram of the p-values.
#

hist(p)

#
#Change the number of bins in the histogram.
#

hist(p,nclass=50)

#
#The p-values were generated from simulated data where only the first 1500
#genes are differentially expressed.
#

hist(p[1:1500])

#
#Note that the p-values for the last 8500 genes look uniform.
#

hist(p[1501:10000])

#
#Load some functions for multiple testing.
#

source("http://www.public.iastate.edu/~dnett/microarray/multtest.txt")

#
#See the names of functions that have been loaded.
#

ls()

#
#Examine the function "estimate.m0" 
#

estimate.m0

#
#Estimate the number of true null hypotheses from the observed p-values.
#

estimate.m0(p)

#
#Examine estimates for other choices of the number of bins.
#

estimate.m0(p,10)

estimate.m0(p,5)

estimate.m0(p,2)

#
#Compute q-values using Benjamini and Hochberg's (1995) method for FDR control.
#

bhq=bh.fdr(p)

#
#Count the number of genes significant at FDR of 0.05.
#

sum(bhq<=0.05)

#
#Count the number of genes significant at FDR of 0.10.
#

sum(bhq<=0.10)

#
#List the gene numbers of those genes significant at FDR 0.10.
#Note in this case we can easily see which results are false
#positives because any gene with an index greater than 1500
#is a false positive.
#

(1:10000)[bhq<=.10]

#
#Now compute q-values using Story and Tibshirani's (2003) method.
#

qvals=q.value(p)

#
#Find a list of genes with approximate FDR 0.10.
#

(1:10000)[qvals<=.10]


#
#Estimate the parameters of a uniform/beta mixture fit to the
#distribution of observed p-values.
#

out=ub.mix(p)

out

#
#Examine the fit of the estimated uniform/beta mixture.
#

plot.ub.mix(p,out)

plot.ub.mix(p,out,50)

#
#In this case, the default starting values seem to produce a
#reasonable fit.  Not all starting values will work well.
#For example ...
#

out=ub.mix(p,c(.6,2,4))

out

plot.ub.mix(p,out)

#
#Now let's compute what Allison et al. (2002) refer to as
#posterior probabilities of differential expression.
#

out=ub.mix(p)

pp=ppde(p,out)

#
#Plot the posterior probability of differential
#expression against the gene index.
#

plot(pp)

#
#Compare the distributions of posterior probabilities
#of differential expression for differentially and
#equivalently expressed genes.
#

par(mfrow=c(2,1))
hist(pp[1:1500],xlab="PPDE",main="PPDE for DE Genes",nclass=20)
hist(pp[-(1:1500)],xlab="PPDE",main="PPDE for EE Genes",nclass=20)

