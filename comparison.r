#' Function \code{make.factor} builds treatment factors for comparing different treatments. Function \code{factor.pc} builds factors for pairwise comparisons of treatments. Function \code{design.pc} builds design matrices for pairwise comparisons of treatments. Function \code{make.contrast} builds a full (ignore constraints) contrast for the comparison of two levels of a factor. Function \code{pairwise} perfoms operations on elements of x pairwise. It's an iterface for general purpose pairwise operations. You can define an operation (\code{fun}) on a pair of elements of a vector. By calling function \code{pairwise}, you get the result of all pairwise operations. 
#' @param trt a treatment factor/vector.
#' @param compare the treatments to be compared.
#' @param lev1 the index of a level of a factor to be comapred with the another level of a factor.
#' @param lev2 the index of another level of the factor. 
#' @param lev.num the number of levels of the factor.
#' @param x a vector whose elements are to be operated on pairwise.
#' @param fun a function which defines the operations to be perfomed on elements of x.
#' @param ... more arguments that be passed to function \code{fun}.
pairwise = function(x,fun,...){
    x.length = length(x)
    result = NULL
    i = 1;
    while(i<x.length){
        j = i + 1
        while(j<=x.length){
            result = c(result,fun(x[i],x[j],...))
            j = j + 1
        }
        i = i + 1
    }
    result
}

make.contrast = function(lev1,lev2,lev.num){
  result = rep(0,lev.num+1)
  result[lev1+1] = 1
  result[lev2+1] = -1
  result
}

design.pc = function(trt){
  if(!is.factor(trt)){
      trt = as.factor(trt)
      warning("argument trt is converted to a factor.")
  }
  trt.levels = levels(trt)
  levels.num = length(trt.levels)
  if(levels.num<=1){
    stop("argument trt must have at least tow levels.")
  }
  pc.num = choose(levels.num,2)
  result = vector('list',pc.num)
  result.name = character(pc.num)
  currentIndex = 1
  for(i in 1:(levels.num-1)){
    for(j in (i+1):levels.num){
      #extract a comparison
      comparison = trt.levels[c(i,j)]
      #build a new factor for the comparison
      trt.factor = maketf(trt,comparison)
      #build the design matrix based on the factor
      dm = model.matrix(~trt.factor)
      result[[currentIndex]] = dm
      result.name[currentIndex] = paste(comparison,collapse="-")
      currentIndex = currentIndex + 1
    }
  }
  names(result) = result.name
  return(result)
}

factor.pc = function(trt){
  if(!is.factor(trt)){
      trt = as.factor(trt)
      warning("argument trt is converted to a factor.")
  }
  trt.levels = levels(trt)
  levels.num = length(trt.levels)
  if(levels.num<=1){
    stop("argument trt must have at least tow levels.")
  }
  result = NULL
  names = NULL
  for(i in 1:(levels.num-1)){
    for(j in (i+1):levels.num){
      compare = trt.levels[c(i,j)]
      tmpResult = maketf(trt,compare)
      result = rbind(result,as.character(tmpResult))
      names=c(names,paste(compare,collapse="-"))
    }
  }
  rownames(result) = names
  return(result)
}

make.factor = function(trt,compare){
  if(!is.factor(trt)){
      trt = as.factor(trt)
      warning("argument trt is converted to a factor.")
  }
  comp.num = length(compare)
  if(comp.num<=1){
    stop("argument compare must be a vector with length greater thant 1.")
  }
  for(i in 2:comp.num){
    trt[trt==compare[i]] = compare[1]
  }
  return(trt)
}
