#' Construct a sub/reduced treatment for testing t1 == t2.
#' @param trt the treatment for the full model.
#' @param t1
#' @param t2
#'
subtrt = function(trt, t1, t2){
    trt[trt==t1] = t2
    trt
}
#' Reduce treatments to fewer levels.
#' @param trt a vector of the original treatments.
#' @param same.trts a list of vectors.
#' Each of the element/vector of the list  contains indices (of trt) 
#' corresponding to treatments that should be treated as the same.
reduce.trt = function(trt, same.trts){
    n = length(same.trts)
    i = 1
    while(i <= n){
        index = same.trts[[i]]
        cat("Original treatments: \n")
        print(trt)
        cat("Index: \n")
        print(index)
        cat("Changed treatments: \n")
        print(trt[index])
        trt[index] = trt[index[1]]
        i = i + 1
    }
    trt
}
#' Format a treatment to avoid possible issues.
#' Basically some special characters are replaced with "_".
format.trt = function(trt){
    # no space, use "_" instead
    gsub(" ", "_", trt, fixed=TRUE) -> trt
    # no "-", use "_" instead
    gsub("-", "_", trt, fixed=TRUE) -> trt
    # no left parentheses, use "_" instead
    gsub("(", "_", trt, fixed=TRUE) -> trt
    # no right parentheses, use "_" instead
    gsub(")", "_", trt, fixed=TRUE) -> trt
    # no "/", use "_" instead
    gsub("/", "_", trt, fixed=TRUE) -> trt
    # no "%", use "_" instead
    gsub("%", "_", trt, fixed=TRUE) -> trt
    trt
}
#' Create a list of design matrices.
dlist = function(trt, trt.first, trt.second){
    if(!all(trt.first %in% trt)){
        stop("All elements of trt.first must be in trt.")
    }
    if(!all(trt.second %in% trt)){
        stop("All elements of trt.second must be in trt.")
    }
    n = length(trt.first)
    if(length(trt.second) != n){
        stop("trt.first and trt.second must have the same length.")
    }
    design.list = vector(n+1, mode="list")
    ff = as.factor(trt)
	# log full factor
	cat("Full factor:\n")
    print(ff)
    cat("\n")
    fm = model.matrix(~ff)
    design.list[[1]] = fm
    for(i in 1:n){
        st = subtrt(trt, trt.first[i], trt.second[i])
        sf = as.factor(st)
		# log sub factor
		cat("Sub factor treating ", trt.first[i], " and ", trt.second[i], " as the same:\n")
        print(sf)
        cat("\n")
        sm = model.matrix(~sf)
        design.list[[i+1]] = sm
    }
    design.list
}

#' Filter genes that have average expression at least 1.
#' @param data
filter.ave1 = function(data){
    rowSums(data) >= ncol(data)
}


#' Filter genes which have expression that make trt comparable to other treatments.
#' @param data a data matrix with rows correspond to genes.
#' @param trts a vector of treatment corresponding to the columns (samples) of the data matrix.
#' @param trt the treatment that is comparable to others.
#filter.comparable = function(data, trts, trt){
#    trts = as.character(trts)
#    trt = as.character(trt)
#    apply(data != 0, 1, function(x){tapply(x,trts,any)}) -> not.zeros
#    rns = row.names(not.zeros)
#    trt.not.zeros = not.zeros[trt,]
#    others.not.zeros = not.zeros[setdiff(rns, trt), , drop=FALSE]
#    trt.not.zeros | apply(others.not.zeros, 2, all)
#}
#' Filter genes which makes each pair of treatments (in trt.first and trt.second) comparable,
#' i.e., genes whose expressions has at least 1 non-zero for each pair of treatments.
#'
filter.comparable = function(data, trt, trt.first, trt.second){
    # convert treatments to character to avoid possible issues
    trt = as.character(trt)
    trt.first = as.character(trt.first)
    trt.second = as.character(trt.second)
    bool = rep(TRUE, nrow(data)) 
    n = length(trt.first)
    for(i in 1:n){
        t1 = trt.first[i]
        t2 = trt.second[i]
        bool = bool & apply(data[, trt==t1 | trt==t2, drop=FALSE]!=0, 1, any)
    }
    bool
}
#' Filter genes based on the average-1 and/or the comparable rule.
filter.genes = function(data, trt, trt.first, trt.second){
    # average 1
	filter.ave1(data) -> i.1
    if(!missing(trt) && !missing(trt.first) && !missing(trt.second)){
        # comparable
        filter.comparable(data, trt, trt.first, trt.second) -> i.2
        i.both = i.1 & i.2
        s.1 = sum(i.1)
        s.both = sum(i.both)
        s.diff = (s.1 - s.both)
        # log diff information so that users can choose whether to use this 
        cat("Number of genes filtered using average-1 rule: ", s.1, "\n", sep="")
        cat("Number of genes filtered using average-1 and comparable rule: ", s.both, "\n", sep="")
        cat("Difference due to the comparable rule: ", s.diff, "\n", sep="")
        cat("Difference (in %) due to the comparable rule: ", round(s.diff/s.1*100, 1), "%\n", sep="")
        return(i.both)
    }
    warning("Genes are filtered based on the average-1 rule only due to missing argument(s).")
    return(i.1)
}
#' Fit the quasi negative binomial model.
#' The offset is automatically set to be 75% of gene expressions in each sample/chip.
#' @param design.list a list of design matrices.
#' The first element must the design matrix for the full model.
#' The function dlist returns such kind of list.
qlfit = function(data, design.list){
    log.offset = log(apply(data, 2, quantile, 0.75))
    QL.fit(data, design.list, log.offset=log.offset, Model="NegBin")
}

pname = function(trt.first, trt.second, prefix="pv", suffix, sep="."){
    if(missing(prefix) || prefix == ""){
        if(missing(suffix) || suffix == ""){
            return(paste(trt.first, trt.second, sep=sep))
        }
        return(paste(trt.first, trt.second, suffix, sep=sep))
    }
    if(missing(suffix) || suffix == ""){
        return(paste(prefix, trt.first, trt.second, sep=sep))
    }
    return(paste(prefix, trt.first, trt.second, suffix, sep=sep))
}
qname = function(trt.first, trt.second, prefix="qv", suffix, sep="."){
    pname(trt.first=trt.first, trt.second=trt.second, prefix=prefix, suffix=suffix, sep=sep)
}
fcname = function(trt.first, trt.second, prefix="fc", suffix, sep="."){
    pname(trt.first=trt.first, trt.second=trt.second, prefix=prefix, suffix=suffix, sep=sep)
}
oname = function(trt.first, trt.second, prefix="out", suffix, sep="."){
    pname(trt.first=trt.first, trt.second=trt.second, prefix=prefix, suffix=suffix, sep=sep)
}
tname = function(trt.first, trt.second, prefix="trt", suffix, sep="."){
    pname(trt.first=trt.first, trt.second=trt.second, prefix=prefix, suffix=suffix, sep=sep)
}
mname = function(trt.first, trt.second, prefix="mod", suffix, sep="."){
    pname(trt.first=trt.first, trt.second=trt.second, prefix=prefix, suffix=suffix, sep=sep)
}
fname = function(trt.first, trt.second, prefix="fac", suffix, sep="."){
    pname(trt.first=trt.first, trt.second=trt.second, prefix=prefix, suffix=suffix, sep=sep)
}
phist = function(p, saveas, ...){
    if(!missing(saveas)){
        pdf(file=saveas)
        hist(x=p, ...)
        dev.off()
        return()
    }
    hist(x=p, ...)
}
trt.estimate = function(qf, trt, t){
    trt.lvls = levels(as.factor(trt))
    index = which(trt.lvls == t)
    if(index==1){
        return(rep(0, nrow(qf$coefficient)))
    }
    return(qf$coefficient[, index])
}
#' Caluclate pvalues, qvalues, fold change and create global variables
#' in the workspace.
#' @param qf A QL.fit object.
#' @param qr A QL.result objet.
pqfc = function(qf, qr, trt, trt.first, trt.second){
    trt.first = as.character(trt.first)
    trt.second = as.character(trt.second)
	pnames = pname(trt.first, trt.second)
    assign(x = "pnames", value = pnames, pos = 1)
	qnames = qname(trt.first, trt.second)
    assign(x = "qnames", value = qnames, pos = 1)
    fcnames = fcname(trt.first, trt.second)
    assign(x = "fcnames", value = fcnames, pos = 1)
    n = length(trt.first)
    for(i in 1:n){
        # pvalues, qvalues and histograms of pvalues
        p = qr$P.values$QLSpline[, i]
        assign(x = pnames[i], value = p, pos = 1)
        q = qr$Q.values$QLSpline[, i]
        assign(x = qnames[i], value = q, pos = 1)
        phist(p=p, saveas=paste(pnames[i], ".pdf", sep=""), main=pnames[i])
        # fold change
        t1 = trt.first[i]
        t2 = trt.second[i]
        fc = exp(trt.estimate(qf, trt, t1) - trt.estimate(qf, trt, t2))
        assign(x = fcnames[i], value = fc, pos = 1)
    }
}
summary.test = function(p, q, fc, data, filter){
    ids = which(filter)
    out = cbind(ids, p, q, fc, data)
    sample.names = colnames(data)
    colnames(out) = c("Row ID", "Pvalue", "Qvalue", "FC", sample.names)
    rownames(out) = rownames(data)
    out
}

#summary.tests = function(data, filter, trt.first, trt.second){
#    pnames = pname(trt.first, trt.second)
#    qnames = qname(trt.first, trt.second)
#    fcnames = fcname(trt.first, trt.second)
#    out = collapse(cbind, pnames, qnames, fcnames)
#    ids = which(filter)
#    out = cbind(ids, out, data)
#    cns = paste(trt.first, trt.second, sep=" VS ")
#    cns = rep(cns, times=3)
#    sample.names = colnames(data)
#    cns = c("Row ID", cns, sample.names)
#    colnames(out) = cns
#    rownames(out) = rownames(data)
#    out
#}
