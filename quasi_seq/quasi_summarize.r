source("/home/dclong/btsync/dclong/code/biostat/quasi_seq/quasi_utility.r")
colnames(data.counts.valid) = trt
# calculate pvalues, qvalues and fold changes
pqfc(qf, qr, trt, trt.first, trt.second)
# summarize each gene
onames = oname(trt.first, trt.second)
tranv(1:length(onames), function(i){
    p = get(pnames[i])
    q = get(qnames[i])
    fc = get(fcnames[i])
    o = summary.test(p, q, fc, data.counts.valid, filter.bool)
    write.csv(o, file=paste(onames[i], ".csv", sep=""), row.names=F)
})
save.image(saveas.file)
# summarize all gene together
# out.all = summary.tests(data, filter, trt.first, trt.second)



