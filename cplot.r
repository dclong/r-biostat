
cplot = function(g1,g2,p1,p2,data,path=getwd()){
  g1 = sort(g1)
  g2 = sort(g2)
  caption = paste(paste(g1,collapse="")," VS ",paste(g2,collapse=""),"\n",p1," VS ",p2,sep="")
  g1 = sort(setdiff(g1,p1))
  g2 = sort(setdiff(g2,p2))
  res1 = data[,p1] - rowMeans(data[,g1])
  res2 = data[,p2] - rowMeans(data[,g2])
  xlabel = paste(p1,"- m(",paste(g1,collapse=","),")")
  ylabel = paste(p2,"- m(",paste(g2,collapse=","),")")
  plot(res1,res2,xlab=xlabel,ylab=ylabel,main=caption,cex=0.3,asp=1)
  abline(h=0,col="blue")
  abline(v=0,col="blue")
  file.name = paste(p1,"-",paste(g1,collapse=""),"_",
                    p2,"-",paste(g2,collapse=""),".pdf",sep="")
  file.name = combinePath(path,file.name)
  if(!file.exists(path)){
    dir.create(path)
  }
  dev.copy2pdf(file=file.name)
}


