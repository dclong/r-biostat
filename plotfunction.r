plot.lsmeans=function (lsmeans,gene) 
{
#
#This function assumes that there is an R object called lsmeans
#that was originally created in SAS and passed to R.
#
#The function's one argument
#is gene that is assigned the id number of the gene for which to
#plot the means and standard error bars.
#
#In this case the treatment factors are called geno and time.  You would
#need to alter the code to get it to work for your situation.
# 

  d=lsmeans[lsmeans$gene==gene,3:6]
  x=d[d$geno=="ML6",2]
  m=d[d$geno=="ML6",3]
  StEr=d[d$geno=="ML6",4]
  up=m+StEr
  low=m-StEr
  
  yrange=range(d$Estimate)
  ll=yrange[1]-mean(StEr)
  uu=yrange[2]+mean(StEr) 
  yrange=c(ll,uu)

 plot(rep(x,3),c(low,m,up),pch=" ",axes=F,
       xlab="time",ylab="Normalized Log Signal",ylim=yrange)
  title(paste("Means + or - One Standard Error\n",
               "gene",gene))
  axis(1,labels=x,at=x)
  axis(2)
  box()
  lines(x,m,lwd=2,lty=1,col=4)
  points(x,m,pch=16)
 



  for(i in 1:length(x)){
    lines(rep(x[i],2),c(low[i],up[i]),lwd=2)
  }
  points(x,low,pch="_")
  points(x,up,pch="_")


  x=d[d$geno=="ML1",2]
  m=d[d$geno=="ML1",3]
  StEr=d[d$geno=="ML1",4]
  up=m+StEr
  low=m-StEr
  points(rep(x,3),c(low,m,up),pch=" ")
  lines(x,m,lwd=2,col=2,lty=2)
  points(x,m,pch=16)
  for(i in 1:length(x)){
    lines(rep(x[i],2),c(low[i],up[i]),lwd=2)
  }
  points(x,low,pch="_")
  points(x,up,pch="_")
legend(locator(1),c("ML6","ML13"),lty=c(1,2),col=c(4,2),lwd=2)
}
