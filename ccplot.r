
ccplot = function(group1,group2,data,path=getwd()){
  if(!file.exists(path)){
    dir.create(path)
  }
  group1 = sort(group1)
  group2 = sort(group2)
  file.name = paste(paste(group1,collapse=""),"-",paste(group2,collapse=""),sep="")
  file.name = combinePath(path,file.name)
  file.name.pdf = paste(file.name,".pdf",sep="")
  file.name.png = paste(file.name,".png",sep="")
  makePlot = function(group1,group2,data){
    par(mfrow=c(length(group1),length(group2)))
    for(i in group1){
        g1 = sort(setdiff(group1,i))
        res1 = data[,i] - rowMeans(as.matrix(data[,g1]))
        for(j in group2){
            g2 = sort(setdiff(group2,j))
            res2 = data[,j] - rowMeans(as.matrix(data[,g2]))
            xlabel = paste(i,"- m(",paste(g1,collapse=","),")")
            ylabel = paste(j,"- m(",paste(g2,collapse=","),")")
            plot(res1,res2,xlab=xlabel,ylab=ylabel,main=paste(i,"VS",j),cex=0.3,asp=1)
            abline(h=0,col="blue")
            abline(v=0,col="blue") 
        }
    }
  }
  pdf(file=file.name.pdf,onefile=FALSE)
  makePlot(group1,group2,data)
  dev.off()
  png(file=file.name.png)
  makePlot(group1,group2,data)
  dev.off()    
}