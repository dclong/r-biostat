
#' Number of Significant Genes

#' Function \code{significant} calculate the number of significant genes. If q values is not specifed, then you must supplied pvalues and specify a method for calculating q values. You can any valid method you want to calculate qvalues. There are several method supplied here. Function \code{q.nettleton} uses the method of Nettleton (2006). Function \code{q.benjamini} calculates q values using the method of Benjimini and Hochberg. Function \code{q.storey} calculates q values using the method of Storey and Tibshirani (2003). All these 3 methods controls the false discover rate (FDR). Generally speaking, the method of Benjimini and Hochberg is more conservative than the method of Nettleton (2006). When there are many potential significant genes, you probably want to use the Benjimini and Hochberg's method which is relatively conseverative, and vice versa.
#' @param fdr the level of false discover rate. Vector is supported.
#' @param p a vector of p values.
#' @param q.method the function to be used to calculate q values.
#' @param q a vector of q values.
#' @param ... more arguments that be passed to function \code{q.method}.
#' @param chip gene chip type, either "microarray" or "rnaseq". Partial values are accepted, i.e. you can use 'm' or 'mic' and so on for "microarray", and similar to "rnaseq".
#' @param B number of bins to use in the histogram. 
#' @param lambda a parameter use by Storey and Tibshirani's method for calculating q values.
#' @export significant qvalues.nettleton estimate.m0 qvalues.benjamini qvalues.storey

significant = function(fdr,p,q.method,q,...){
    #calculate q values using the specified method 
    if(missing(q)){
      q = q.method(p=p,...)
    }
    if(is.null(q)){
      return(NULL)
    }
    if(is.vector(q,'numeric')){
      q = as.matrix(q)
    }
    col.num = ncol(q)
    result = matrix(0,nrow=length(fdr),ncol=col.num)
    rownames(result) = fdr
    for(i in 1:col.num){
      result[,i] = apply(outer(q[,i],fdr,"<="),2,sum)
    }
    colnames(result) = colnames(q)
    return(result)
}

q.storey=function(p, lambda=seq(0,0.95,0.05)) {
#
#This is Storey and Tibshirani's (PNAS, 2003) default method
#for determining the q-values.
#
#Code was originally obtained from John Storey's Web site.
#It has been edited slightly.
#
  m<-length(p) 
  pi0 <- rep(0,length(lambda))
  for(i in 1:length(lambda)) {
    pi0[i] <- mean(p >= lambda[i])/(1-lambda[i])
  }
  spi0 <- smooth.spline(lambda,pi0,df=3)
  pi0 <- max(predict(spi0,x=1)$y,0)
  pi0 <- min(pi0,1)
  u <- order(p)
  v <- rank(p)
  qvalue <- pi0*m*p/v
  qvalue[u[m]] <- min(qvalue[u[m]],1)
  for(i in (m-1):1) {
    qvalue[u[i]] <- min(qvalue[u[i]],qvalue[u[i+1]],1)
  }
  return(qvalue)
}

q.benjamini=function(p)
{
#
#This function computes q-values using Benjamini and Hochberg's (1995)
#approach for controlling FDR.
#
  m = length(p)
  k = 1:m
  ord = order(p)
  p[ord] = (p[ord] * m)/(1:m)
  qval = p
  qval[ord]=rev(cummin(rev(qval[ord])))
  return(qval)
}

q.nettleton=function(p,chip,B=20)
{
#
#This function computes q-values using the approach of Nettleton et al.
#(2006) JABES 11, 337-356.
#
#Author: Dan Nettleton
#
  m = length(p)
  m0 = estimate.m0(p=p,chip=chip,B=B)
  k = 1:m
  ord = order(p)
  p[ord] = (p[ord] * m0)/(1:m)
  qval = p
  qval[ord]=rev(cummin(rev(qval[ord])))
  return(qval)
}

estimate.m0 = function(p,chip,B=20){
  chip.types = c("microarray","rnaseq")
  chip = tolower(chip)
  chip.types[pmatch(chip,chip.types)]->chip
  if(chip=="microarray"){
    return(estimate.m0.microarray(p=p,B=B))
  }
  else{
    return(estimate.m0.rnaseq(p=p,B=B))
  }
}

estimate.m0.rnaseq=function(p, B = 20)
{
#MODIFIED FOR CASE OF MANY P-VALUES IN LAST BIN.
#This function estimates the number of true null hypotheses given a vector of p-values
#using the method of Nettleton et al. (2006) JABES 11, 337-356.
#The estimate obtained is identical to the estimate obtained by the iterative
#procedure described by Mosig et al. Genetics 157:1683-1698.
#The number of p-values falling into B equally sized bins are counted.
#The count of each bin is compared to the average of all the bin counts associated
#with the current bins and all bins to its right.  Working from left to right, 
#the first bin that has a count less than or equal to the average is identified.
#That average is multiplied by the total number of bins to obtain an estimate of m0, 
#the number of tests for which the null hypothesis is true.
#
  m <- length(p)
  m0 <- m
  bin <- c(-0.1, (1:B)/B)
  bin.counts=rep(0,B)
  for(i in 1:B){
    bin.counts[i]=sum((p>bin[i])&(p<=bin[i+1]))
  }
  tail.means <- rev(cumsum(rev(bin.counts[-B]))/(1:(B-1)))
  temp <- bin.counts[-B] - tail.means
  index <- min((1:(B-1))[temp <= 0])
  m0 <- (B-1) * tail.means[index]+bin.counts[B]
  return(m0)
}

estimate.m0.microarray=function(p, B = 20)
{
#
#This function estimates the number of true null hypotheses given a vector of p-values
#using the method of Nettleton et al. (2006) JABES 11, 337-356.
#The estimate obtained is identical to the estimate obtained by the iterative
#procedure described by Mosig et al. Genetics 157:1683-1698.
#The number of p-values falling into B equally sized bins are counted.
#The count of each bin is compared to the average of all the bin counts associated
#with the current bins and all bins to its right.  Working from left to right, 
#the first bin that has a count less than or equal to the average is identified.
#That average is multiplied by the total number of bins to obtain an estimate of m0, 
#the number of tests for which the null hypothesis is true.
#
  m <- length(p)
  m0 <- m
  bin <- c(-0.1, (1:B)/B)
  bin.counts=rep(0,B)
  for(i in 1:B){
    bin.counts[i]=sum((p>bin[i])&(p<=bin[i+1]))
  }
  tail.means <- rev(cumsum(rev(bin.counts))/(1:B))
  temp <- bin.counts - tail.means
  index <- min((1:B)[temp <= 0])
  m0 <- B * tail.means[index]
  return(m0)
}
